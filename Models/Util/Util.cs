﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeUtility;

namespace Models.Util
{
    public static class Util
    {
        public static string ToKeywordSearch(this object obj)
        {
            return obj.ToLowerCase().ToNoSignFormat();
        }
    }
}

namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bill")]
    public partial class Bill
    {
        public int BillID { get; set; }

        public int? Position { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        public bool? Status { get; set; }

        public double? Total { get; set; }

        public double? TotalAmount { get; set; }

        public int? Bonus { get; set; }

        [StringLength(255)]
        public string CreateBy { get; set; }

        public int? TableFoodID { get; set; }
    }
}

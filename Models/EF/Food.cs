namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Food")]
    public partial class Food
    {
        public int FoodID { get; set; }

        [StringLength(255)]
        public string FoodName { get; set; }

        [StringLength(255)]
        public string Avatar { get; set; }

        public double? Price_Import { get; set; }

        public double? Price_Output { get; set; }

        public int? Quantity { get; set; }

        public int? Position { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        public bool? Status { get; set; }

        [StringLength(255)]
        public string CreateBy { get; set; }

        public int? FoodTypeID { get; set; }
    }
}

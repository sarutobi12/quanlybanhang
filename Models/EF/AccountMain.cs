namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccountMain")]
    public partial class AccountMain
    {
        public int AccountMainID { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        [StringLength(500)]
        public string Avatar { get; set; }

        public int? Position { get; set; }

        public bool? Status { get; set; }

        [StringLength(50)]
        public string Default_Url { get; set; }
    }
}

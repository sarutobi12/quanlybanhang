namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderHistory")]
    public partial class OrderHistory
    {
        public int ID { get; set; }

        public int? FoodID { get; set; }

        public int? OldQuantity { get; set; }
        public int? NewQuantity { get; set; }

        public int? TableID { get; set; }
        public int? NewTableID { get; set; }

        [StringLength(255)]
        public string AccountOrder { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }
        public bool? Status { get; set; }
    }
}

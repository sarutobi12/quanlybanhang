namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Account")]
    public partial class Account
    {
        [Key]
        [StringLength(255)]
        public string Username { get; set; }

        [StringLength(255)]
        public string Password { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(50)]
        public string FullName { get; set; }

        public DateTime? BirthDay { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Avatar { get; set; }

        [StringLength(20)]
        public string Mobi { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        [StringLength(255)]
        public string CreateBy { get; set; }

        public int? Position { get; set; }

        public bool? Status { get; set; }

        [StringLength(500)]
        public string QRCodeHass { get; set; }

        public int? AccountMainID { get; set; }
    }
}

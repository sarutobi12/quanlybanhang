namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FoodImport")]
    public partial class FoodImport
    {
        public int FoodImportID { get; set; }

        public double? Total { get; set; }

        public DateTime? CreateTime { get; set; }

        [StringLength(255)]
        public string CreateBy { get; set; }
    }
}

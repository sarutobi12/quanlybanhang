﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class ChartFoodProductModel
    {
        public int idFood { get; set; }
        public string FoodName { get; set; }
        public int? Quantity { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.ViewModel
{
  public class CartItem
  {
    public int ID { get; set; }
    public string FoodName { get; set; }
    public int OldQuantity { get; set; }
    public int NewQuantity { get; set; }
    public int TotalQuantity
    {
      get
      {
        return OldQuantity + NewQuantity;
      }
    }
    public string Description { get; set; }
    public double PriceImport { get; set; }
    public double Total
    {
      get
      {
        return PriceImport * NewQuantity;
      }
    }
  }
}

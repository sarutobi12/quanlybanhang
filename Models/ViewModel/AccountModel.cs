﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class AccountModel
    {
        public string Username { get; set; }

        public string Password { get; set; }


        public string Email { get; set; }


        public string FullName { get; set; }

        public DateTime? BirthDay { get; set; }


        public string Address { get; set; }


        public string Avatar { get; set; }


        public string Mobi { get; set; }


        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        public string CreateBy { get; set; }

        public int? Position { get; set; }

        public bool? Status { get; set; }


        public string QRCodeHass { get; set; }

        public int AccountMainID { get; set; }

        public string AccountMainTitle { get; set; }
    }
}

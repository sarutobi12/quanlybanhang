﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class FoodModel
    {
        public int FoodID { get; set; }


        public string FoodName { get; set; }


        public string Avatar { get; set; }

        public double? Price_Import { get; set; }

        public double? Price_Output { get; set; }

        public int? Quantity { get; set; }

        public int? Position { get; set; }


        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        public bool? Status { get; set; }

        public string CreateBy { get; set; }

        public int? FoodTypeID { get; set; }

        public string FoodTypeName { get; set; }
    }
}

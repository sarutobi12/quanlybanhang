﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class ChartModel
    {
        public string name { get; set; }
        public double y { get; set; }
        public string drilldown { get; set; }
    }
}

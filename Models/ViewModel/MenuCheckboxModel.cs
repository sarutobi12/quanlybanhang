﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class MenuCheckboxModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public bool isCheck { get; set; }
    }
}

﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class Table
    {
        public int TableID { get; set; }

        public string TableName { get; set; }

        public int? Position { get; set; }

        public string Description { get; set; }

        public DateTime? CreateTime { get; set; }

        public bool? Status { get; set; }

    }
}

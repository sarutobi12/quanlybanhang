using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.ViewModel
{
  public class BillDetailPrintModel
  {
    public int FoodID { get; set; }
    public string FoodName { get; set; }
    public int? Quantity { get; set; }
    public double? Price { get; set; }
    public double? Amount { get; set; }
  }
}

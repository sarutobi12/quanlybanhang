using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.Models
{
   public class LoginModal
  {
    public AccountModel Account { get; set; }
    public int Result { get; set; }
  }
}

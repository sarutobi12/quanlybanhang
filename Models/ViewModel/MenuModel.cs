﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
   public class MenuModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Name { get; set; }


        public string Path { get; set; }

        public bool? Status { get; set; }

        public string Icon { get; set; }

        public int? Role { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.ViewModel
{
    public class ImportHistoryModel
    {
        public int FoodImportID { get; set; }

        public double? Total { get; set; }

        public DateTime? CreateTime { get; set; }

        public string NameUserImport { get; set; }

        public int QuantityFoodImport { get; set; }
        public int ToTalQuantityFoodImport { get; set; }

        public string CreateBy { get; set; }
    }
}

using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.ViewModel
{
    public class BillPrintModel
    {
        public int BillID { get; set; }


        public DateTime? CreateTime { get; set; }


        public double? Total { get; set; }

        public double? TotalAmount { get; set; }

        public int? Bonus { get; set; }

        public int? TableFoodID { get; set; }

        public List<BillDetailPrintModel> BillDetail { get; set; }
    }
}

﻿using CodeUtility;
using Models.EF;
using Models.Models;
using Models.Util;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class AccountDao
    {
        DBContext db = null;

        public AccountDao()
        {
            db = new DBContext();
        }

        public AccountModel GetAccountByID(string username)
        {
            var data = db.Accounts
                .Join(db.AccountMains, a => a.AccountMainID, am => am.AccountMainID, (a, am) => new AccountModel
                {
                    AccountMainID = am.AccountMainID,
                    AccountMainTitle = am.Title,
                    Address = a.Address,
                    Avatar = a.Avatar,
                    BirthDay = a.BirthDay,
                    CreateTime = a.CreateTime,
                    Description = a.Description,
                    Email = a.Email,
                    FullName = a.FullName,
                    Mobi = a.Mobi,
                    Password = a.Password,
                    Position = a.Position,
                    QRCodeHass = a.QRCodeHass,
                    Status = a.Status,
                    Username = a.Username
                }).SingleOrDefault(x => x.Username == username);
            return data;
        }

        public int Login(string username, string password)
        {
            Account item = db.Accounts.Find(username);
            if (item == null)
            {
                return -1;
            }
            else
            {
                if (item.Status == false)
                {
                    return 0;
                }
                if (item.Password.ToLower() != password.ToLower())
                {
                    return 1;
                }
                return 2;
            }
        }

        public LoginModal LoginByQrCode(string hass)
        {
            AccountModel item = db.Accounts
             .Join(db.AccountMains, a => a.AccountMainID, am => am.AccountMainID, (a, am) => new AccountModel
             {
                 AccountMainID = am.AccountMainID,
                 AccountMainTitle = am.Title,
                 Address = a.Address,
                 Avatar = a.Avatar,
                 BirthDay = a.BirthDay,
                 CreateTime = a.CreateTime,
                 Description = a.Description,
                 Email = a.Email,
                 FullName = a.FullName,
                 Mobi = a.Mobi,
                 Password = a.Password,
                 Position = a.Position,
                 QRCodeHass = a.QRCodeHass,
                 Status = a.Status,
                 Username = a.Username
             }).SingleOrDefault(x => x.QRCodeHass == hass);
            if (item == null)
            {
                return new LoginModal
                {
                    Result = -1
                };
            }
            else
            {
                if (item.Status == false)
                {
                    return new LoginModal
                    {
                        Result = 0
                    };
                }
                return new LoginModal
                {
                    Account = item,
                    Result = 1
                };
            }
        }

        public string GetDefaultUrlAccount(int idAccountMain)
        {
            return db.AccountMains.SingleOrDefault(x => x.AccountMainID == idAccountMain).Default_Url;
        }

        public List<Account> GetAllAccount()
        {
            return db.Accounts.ToList();
        }

        public int GetCountAccount()
        {
            return db.Accounts.ToList().Count;
        }

        public object InsertAccount(string username, string password, int accountMainID, string fullName, string address, string mobi, string email, bool status, string hassQRCode, AccountModel accountModel)
        {

            var checkAccount = db.Accounts.Find(username);

            if (checkAccount != null)
            {
                return new
                {
                    status = 0,
                    message = "Tài khoản đã tồn tại! Vui lòng chọn tài khoản khác"
                };

            }

            Account account = new Account();
            account.Username = username;
            account.Password = password;
            account.AccountMainID = accountMainID;
            account.FullName = fullName;
            account.Address = address;
            account.Mobi = mobi;
            account.Email = email;
            account.CreateTime = DateTime.Now;
            account.CreateBy = accountModel.Username;
            account.Position = 0;
            account.Status = status;
            account.QRCodeHass = hassQRCode;
            db.Accounts.Add(account);
            try
            {
                db.SaveChanges();

                return new
                {
                    status = 1,
                    message = "Bạn đã thêm tài khoản thành công"
                };
            }
            catch
            {
                return new
                {
                    status = 0,
                    message = "Lỗi hệ thống! Vui lòng thử lại"
                };
            }
        }

        public object UpdateAccount(string username, string password, int accountMainID, string fullName, string address, string mobi, string email, bool status, string hassQRCode, AccountModel accountModel)
        {
            var itemAccount = db.Accounts.FirstOrDefault(x => x.Username == username);
            if (itemAccount != null)
            {
                itemAccount.Username = username;
                itemAccount.AccountMainID = accountMainID;
                itemAccount.FullName = fullName;
                itemAccount.Address = address;
                itemAccount.Mobi = mobi;
                itemAccount.Email = email;
                itemAccount.Status = status;
                itemAccount.CreateBy = accountModel.Username;
                if (password != string.Empty)
                {
                    itemAccount.Password = password;
                }
                if (hassQRCode != string.Empty)
                {
                    itemAccount.QRCodeHass = hassQRCode;
                }
                try
                {
                    db.SaveChanges();

                    return new
                    {
                        status = 1,
                        message = "Bạn đã cập nhật tài khoản thành công"
                    };
                }
                catch
                {
                    return new
                    {
                        status = 0,
                        message = "Lỗi hệ thống! Vui lòng thử lại"
                    };
                }
            }



            return new
            {
                status = 0,
                message = "Không tìm thấy tài khoản! Vui lòng thử lại"
            };
        }

        public object RemoveAccount(string username)
        {
            var itemAccount = db.Accounts.Find(username);
            if (itemAccount != null)
            {
                db.Accounts.Remove(itemAccount);

                try
                {
                    db.SaveChanges();

                    return new
                    {
                        status = 1,
                        message = "Bạn đã xóa tài khoản thành công"
                    };
                }
                catch
                {
                    return new
                    {
                        status = 0,
                        message = "Lỗi hệ thống! Vui lòng thử lại"
                    };
                }
            }

            return new
            {
                status = 0,
                message = "Không tìm thấy tài khoản! Vui lòng thử lại"
            };
        }

        public object SearchAccount(int accountMainID, string keyword, int page = 1)
        {
            //Cấu hình page
            int pageSize = 5;
            IEnumerable<AccountModel> data = db.Accounts.Join(db.AccountMains, a => a.AccountMainID, m => m.AccountMainID, (a, m) => new AccountModel
            {
                Username = a.Username,
                Password = a.Password,
                FullName = a.FullName,
                Address = a.Address,
                Avatar = a.Avatar,
                BirthDay = a.BirthDay,
                CreateTime = a.CreateTime,
                Description = a.Description,
                Email = a.Email,
                Mobi = a.Mobi,
                Position = a.Position,
                Status = a.Status,
                QRCodeHass = a.QRCodeHass,
                AccountMainID = a.AccountMainID.Value,
                AccountMainTitle = m.Title

            });
            if (accountMainID != 0)
            {
                data = data.Where(x => x.AccountMainID == accountMainID);
            }
            if (keyword != string.Empty)
            {
                data = data.Where(x => x.Username.ToKeywordSearch().Contains(keyword.ToKeywordSearch())
                || x.FullName.ToKeywordSearch().Contains(keyword.ToKeywordSearch())
                || x.Address.ToKeywordSearch().Contains(keyword.ToKeywordSearch())
                || x.Mobi.ToKeywordSearch().Contains(keyword.ToKeywordSearch())
                || x.Email.ToKeywordSearch().Contains(keyword.ToKeywordSearch())
                );
            }

            int countItem = data.Count();
            double totalPage = Math.Ceiling((double)countItem / pageSize);

            var currenPage = page;
            if (totalPage > 0)
            {
                if (currenPage < 1)
                    currenPage = 1;

                if (currenPage > totalPage)
                    currenPage = totalPage.ToInt();
            }

            int skip = (currenPage - 1) * pageSize;
            var result = data.OrderBy(x => x.Position).Skip(skip).Take(pageSize).ToList();

            int totalRecore = GetAllAccount().Count();
            object model = new
            {
                totalPage,
                result,
                skip,
                countItem = totalRecore
            };

            return model;

        }
    }
}

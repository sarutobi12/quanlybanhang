﻿using CodeUtility;
using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class ImportDao
    {
        DBContext db = null;

        public ImportDao()
        {
            db = new DBContext();
        }

        public bool AddImport(Cart cart,AccountModel accountModel)
        {
            if (cart != null)
            {

                try
                {
                    FoodImport foodImport = new FoodImport();
                    foodImport.CreateTime = DateTime.Now;
                    foodImport.Total = cart.Amount;
                    foodImport.CreateBy =accountModel.Username;

                    db.FoodImports.Add(foodImport);
                    db.SaveChanges();

                    var dataImportDetail = cart.CartItems.Values;
                    foreach (var itemDetail in dataImportDetail)
                    {
                        FoodImportDetail foodImportDetail = new FoodImportDetail();
                        foodImportDetail.FoodImportID = foodImport.FoodImportID;
                        foodImportDetail.FoodID = itemDetail.ID;
                        foodImportDetail.Amount = itemDetail.Total;
                        foodImportDetail.Quantity = itemDetail.NewQuantity;
                        db.FoodImportDetails.Add(foodImportDetail);
                        db.SaveChanges();
                    }

                    return true;
                }
                catch
                {

                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public object GetImportDetailByID(int importFoodID)
        {
            var data = db.FoodImportDetails.Join(db.Foods, fd => fd.FoodID, f => f.FoodID, (fd, f) => new ImportDetailHistoryModel
            {
                FoodID = fd.FoodID,
                Amount = fd.Amount,
                FoodImportID = fd.FoodImportID,
                FoodName = f.FoodName,
                Quantity = fd.Quantity
            }).Where(x=>x.FoodImportID == importFoodID).ToList();
            return data;
        }


        public object ImportHistory(string fromDate, string endDate, int page = 1)
        {
            //Cấu hình page
            int pageSize = 5;
            //var data = db.FoodImports
            //    .GroupJoin(db.Accounts, i => i.CreateBy, a => a.Username, (i, a) => new { FoodImport = i, Account = a })
            //    .SelectMany(a => a.Account.DefaultIfEmpty(),
            //    (i, a) => new ImportHistoryModel
            //    {
            //        FoodImportID = i.FoodImport.FoodImportID,
            //        CreateBy = i.FoodImport.CreateBy,
            //        CreateTime = i.FoodImport.CreateTime,
            //        NameUserImport = a.FullName,
            //        QuantityFoodImport = db.FoodImportDetails.Count(x => x.FoodImportID == i.FoodImport.FoodImportID),
            //        Total = i.FoodImport.Total
            //    });


            var data = from i in db.FoodImports
                       join b in db.Accounts on i.CreateBy equals b.Username into us
                       from x in us.DefaultIfEmpty()
                       select new ImportHistoryModel
                       {
                           FoodImportID = i.FoodImportID,
                           CreateBy = i.CreateBy,
                           CreateTime = i.CreateTime,
                           NameUserImport = x.FullName,
                           QuantityFoodImport = db.FoodImportDetails.Count(z => z.FoodImportID == i.FoodImportID),
                           ToTalQuantityFoodImport = db.FoodImportDetails.Where(z => z.FoodImportID == i.FoodImportID).Sum(z => z.Quantity.Value),
                           Total = i.Total
                       };
            if (fromDate != string.Empty && endDate != string.Empty)
            {
                DateTime fromDateConvert = Convert.ToDateTime(fromDate).Date;
                DateTime endDateConvert = Convert.ToDateTime(endDate).Date;

                data = data.Where(x => DbFunctions.TruncateTime(x.CreateTime) >=fromDateConvert
                && DbFunctions.TruncateTime(x.CreateTime)<= endDateConvert);

            }

            int countItem = data.Count();

            double totalMoneyImport =data.Sum(x=>x.Total.Value);


            double totalPage = Math.Ceiling((double)countItem / pageSize);

            var currenPage = page;
            if (totalPage > 0)
            {
                if (currenPage < 1)
                    currenPage = 1;

                if (currenPage > totalPage)
                    currenPage = totalPage.ToInt();
            }

            int skip = (currenPage - 1) * pageSize;
            var result = data.OrderByDescending(x => x.CreateTime).Skip(skip).Take(pageSize).ToList();

            object model = new
            {
                totalPage,
                result,
                skip,
                countItem,
                totalMoneyImport
            };

            return model;


        }
    }
}

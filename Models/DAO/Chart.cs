﻿using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeUtility;

namespace Models.DAO
{
    public class Chart
    {
        DBContext db = null;

        public Chart()
        {
            db = new DBContext();
        }

        public object GetDataChart(string fromDate, string endDate)
        {
            IEnumerable<string> dates = null;
            if (fromDate != string.Empty && endDate != string.Empty)
            {
                DateTime d1 = fromDate.ToDateTime();
                DateTime d2 = endDate.ToDateTime();
                dates = GetDates(d1, d2);
            }
            else
            {
                DateTime today = DateTime.Today;
                int currentDayOfWeek = (int)today.DayOfWeek;
                DateTime sunday = today.AddDays(-currentDayOfWeek);
                DateTime monday = sunday.AddDays(1);
                if (currentDayOfWeek == 0)
                {
                    monday = monday.AddDays(-7);
                }

                dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days).ToString("yyyy-MM-dd")).ToList();
            }

            List<ChartModel> listData = new List<ChartModel>();
            List<ChartListTableModel> chartListTableModels = new List<ChartListTableModel>();
            foreach (var item in dates)
            {
                List<object[]> detailTable = new List<object[]>();
                DateTime d = Convert.ToDateTime(item);
                string date = d.Day + "/" + d.Month + "/" + d.Year;

                double totalByDate = 0;
                var dataGroupBillByTableID = db.Bills.Where(x => DbFunctions.TruncateTime(x.CreateTime) == d)
                                .GroupBy(x => x.TableFoodID).Select(x => new
                                {
                                    tableFoodID = x.Key,
                                    listBill = x.ToList()
                                }).ToList();

                foreach (var itemTable in dataGroupBillByTableID)
                {
                    double totalAmountDetail = 0;
                    foreach (var itemBill in itemTable.listBill)
                    {

                        totalByDate += itemBill.Total.Value;

                        totalAmountDetail += itemBill.Total.Value;
                    }

                    string tableName = db.TableFoods.FirstOrDefault(x => x.TableID == itemTable.tableFoodID).TableName;
                    object[] dataTable = new object[2];
                    dataTable[0] = tableName;
                    dataTable[1] = totalAmountDetail;

                    detailTable.Add(dataTable);
                }
                //Lấy dữ liệu chi tiết bàn
                ChartListTableModel chartListTableModel = new ChartListTableModel
                {
                    name = "Ngày :" + date,
                    id = date,
                    data = detailTable
                };
                chartListTableModels.Add(chartListTableModel);

                ChartModel chartModel = new ChartModel
                {
                    name = date,
                    y = totalByDate,
                    drilldown = date
                };
                listData.Add(chartModel);


            }

            fromDate = dates.FirstOrDefault();
            endDate = dates.LastOrDefault();
            return new
            {
                listData,
                chartListTableModels,
                fromDate,
                endDate
            };
        }


        public IEnumerable<string> GetDates(DateTime startDate, DateTime endDate)
        {
            List<string> dates = new List<string>();

            while (startDate <= endDate)
            {
                dates.Add(startDate.ToString("yyyy-MM-dd"));

                startDate = startDate.AddDays(1);
            }



            return dates;
        }

        public object GetDataChartProduct(string fromDate, string endDate)
        {
            IEnumerable<string> dates = null;
            if (fromDate != string.Empty && endDate != string.Empty)
            {
                DateTime d1 = fromDate.ToDateTime();
                DateTime d2 = endDate.ToDateTime();
                dates = GetDates(d1, d2);
            }
            else
            {
                DateTime today = DateTime.Today;
                int currentDayOfWeek = (int)today.DayOfWeek;
                DateTime sunday = today.AddDays(-currentDayOfWeek);
                DateTime monday = sunday.AddDays(1);
                if (currentDayOfWeek == 0)
                {
                    monday = monday.AddDays(-7);
                }

                dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days).ToString("yyyy-MM-dd")).ToList();
            }

            List<string> listCategory = new List<string>();

            Dictionary<int,ChartFoodProductModel> listAllFood = new Dictionary<int, ChartFoodProductModel>();
            foreach (var item in dates)
            {
                DateTime d = Convert.ToDateTime(item);

                var dataBill= db.Bills.Where(x => DbFunctions.TruncateTime(x.CreateTime) == d).ToList();

                ChartFoodProductModel chartFoodProductModel;
                foreach (var itemBill in dataBill)
                {
                    var dataBillDetail = db.BillDetails.Where(x => x.BillID == itemBill.BillID).ToList();
                    foreach (var itemBillDetail in dataBillDetail)
                    {
                        if (listAllFood.ContainsKey(itemBillDetail.FoodID))
                        {
                            chartFoodProductModel = listAllFood[itemBillDetail.FoodID];
                            chartFoodProductModel.Quantity += itemBillDetail.Quantity;
                        }
                        else
                        {
                            chartFoodProductModel = new ChartFoodProductModel();
                            chartFoodProductModel.idFood = itemBillDetail.FoodID;
                            chartFoodProductModel.Quantity = itemBillDetail.Quantity;
                            chartFoodProductModel.FoodName = new FoodDao().GetFoodByID(itemBillDetail.FoodID).FoodName;

                            listAllFood.Add(itemBillDetail.FoodID, chartFoodProductModel);
                        }
                    }
                   
                   
                }
            }

            var listFoodChart = listAllFood.OrderByDescending(x=>x.Value.Quantity).Take(10);

            fromDate = dates.FirstOrDefault();
            endDate = dates.LastOrDefault();
            return new
            {
                listFoodChart
            };
        }


    }
}

﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.DAO
{
    public class AccountMainDao
    {
        private DBContext db = null;

        public AccountMainDao()
        {
            db = new DBContext();
        }

        public List<AccountMain> GetAllAccountMains()
        {
            return db.AccountMains.OrderBy(x=>x.Position).ToList();
        }

        public AccountMain GetAcountMainByID(int accountMainID)
        {
            return db.AccountMains.Find(accountMainID);
        }

        public object InsertAccountMain(string title, int position)
        {
            AccountMain accountMain = new AccountMain();
            accountMain.Title = title;
            accountMain.Position = position;
            accountMain.CreateTime = DateTime.Now;
            accountMain.Status = true;

            db.AccountMains.Add(accountMain);
            try
            {
                db.SaveChanges();

                return new
                {
                    status = 1,
                    message = "Bạn đã thêm loại tài khoản thành công"
                };
            }
            catch
            {
                return new
                {
                    status = 0,
                    message = "Lỗi hệ thống! Vui lòng thử lại"
                };
            }
        }

        public object UpdateAccountMain(int accountMainID, string title, int position)
        {
            var itemAccountMain = db.AccountMains.Find(accountMainID);
            if (itemAccountMain != null)
            {
                itemAccountMain.Title = title;
                itemAccountMain.Position = position;

                try
                {
                    db.SaveChanges();

                    return new
                    {
                        status = 1,
                        message = "Bạn đã cập nhật loại tài khoản thành công"
                    };
                }
                catch
                {
                    return new
                    {
                        status = 0,
                        message = "Lỗi hệ thống! Vui lòng thử lại"
                    };
                }
            }

            return new
            {
                status = 0,
                message = "Không tìm thấy loại tài khoản! Vui lòng thử lại"
            };
        }

        public object RemoveAccountMain(int accountMainID)
        {
            var itemAccountMain = db.AccountMains.Find(accountMainID);
            if (itemAccountMain != null)
            {
                db.AccountMains.Remove(itemAccountMain);

                try
                {
                    db.SaveChanges();

                    return new
                    {
                        status = 1,
                        message = "Bạn đã xóa loại tài khoản thành công"
                    };
                }
                catch
                {
                    return new
                    {
                        status = 0,
                        message = "Lỗi hệ thống! Vui lòng thử lại"
                    };
                }
            }

            return new
            {
                status = 0,
                message = "Không tìm thấy loại tài khoản! Vui lòng thử lại"
            };
        }
    }
}
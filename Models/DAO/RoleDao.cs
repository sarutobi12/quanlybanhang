﻿using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class RoleDao
    {
        DBContext db = null;
        public RoleDao()
        {
            db = new DBContext();
        }

        public List<Role> GetAllRole()
        {
            return db.Roles.ToList();
        }
        }
}

﻿using CodeUtility;
using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.DAO
{
    public class TableFoodDao
    {
        private DBContext db = null;

        public TableFoodDao()
        {
            db = new DBContext();
        }

        public List<TableFood> getAllTableFood()
        {
            return db.TableFoods.ToList();
        }

        public List<TableFood> getAllTableFoodEmpty()
        {
            return db.TableFoods.Where(x => x.Status == false).ToList();
        }

        public List<TableFood> getAllTableFoodNotEmpty()
        {
            return db.TableFoods.Where(x => x.Status == true).ToList();
        }

        public TableFood getTableByID(int id)
        {
            return db.TableFoods.Find(id);
        }

        public object Order(int tableID, int idFood, string description, int quantity, AccountModel account)
        {
            //Kiểm tra coi food còn hàng hay không
            var dataFood = new FoodDao().GetFoodByID(idFood);
            if (dataFood != null)
            {
                if (dataFood.Quantity < quantity)
                {
                    if (dataFood.Quantity > 0)
                    {
                        object result = new
                        {
                            statusFood = -1,
                            quantity = dataFood.Quantity
                        };
                        return result;
                    }
                    else
                    {
                        object result = new
                        {
                            statusFood = -2
                        };
                        return result;
                    }
                }
                else
                {
                    object results;
                    var findFoodTable = db.TableFoodDetails.FirstOrDefault(x => x.TableFoodID == tableID && x.FoodID == idFood);
                    //Cập nhật số lượng nếu food đã tồn tại trong bàn
                    if (findFoodTable != null)
                    {
                        //Add OrderHistory
                        OrderHistory orderHistory = new OrderHistory();
                        orderHistory.FoodID = idFood;
                        orderHistory.TableID = tableID;
                        orderHistory.OldQuantity = findFoodTable.Quantity;
                        orderHistory.NewQuantity = findFoodTable.Quantity + quantity;
                        orderHistory.Description = "edit";
                        orderHistory.CreateTime = DateTime.Now;
                        orderHistory.AccountOrder = account.Username;
                        orderHistory.Status = false;
                        db.OrderHistories.Add(orderHistory);
                        db.SaveChanges();

                        findFoodTable.Quantity += quantity;
                        findFoodTable.Description = description;
                        findFoodTable.Amount = findFoodTable.Quantity * new FoodDao().GetPriceByIDFood(idFood);
                        db.SaveChanges();
                        //Update lại số lượng;
                        UpdateQuantityFood(idFood, quantity, "decrease");
                        //Cập nhât lại bàn
                        UpdateDataTableFood(tableID);

                       

                        results = new
                        {
                            statusFood = 1,
                            result = findFoodTable
                        };
                        return results;
                    }
                    else
                    {
                        //Cập nhật chi tiết món mới
                        TableFoodDetail tableFoodDetail = new TableFoodDetail();
                        tableFoodDetail.TableFoodID = tableID;
                        tableFoodDetail.FoodID = idFood;
                        tableFoodDetail.Quantity = quantity;
                        tableFoodDetail.Description = description;
                        tableFoodDetail.Amount = quantity * new FoodDao().GetPriceByIDFood(idFood);

                        db.TableFoodDetails.Add(tableFoodDetail);
                        db.SaveChanges();

                        //Add OrderHistory
                        OrderHistory orderHistory = new OrderHistory();
                        orderHistory.FoodID = idFood;
                        orderHistory.TableID = tableID;
                        orderHistory.OldQuantity = 0;
                        orderHistory.NewQuantity = quantity;
                        orderHistory.Description = "add";
                        orderHistory.CreateTime = DateTime.Now;
                        orderHistory.AccountOrder = account.Username;
                        orderHistory.Status = false;
                        db.OrderHistories.Add(orderHistory);
                        db.SaveChanges();
                        //Update lại số lượng;
                        UpdateQuantityFood(idFood, quantity, "decrease");
                        //Cập nhât lại bàn
                        UpdateDataTableFood(tableID);

                        object result = new
                        {
                            statusFood = 1,
                            result = tableFoodDetail
                        };
                        return result;
                    }
                }
            }
            else
                return new
                {
                    statusFood = 0,
                };
        }

        public List<TableFoodDetailModel> GetListOrderByTable(int tableID)
        {
            var listFoodDetail = db.TableFoodDetails.Where(x => x.TableFoodID == tableID).ToList();

            List<TableFoodDetailModel> listDetail = new List<TableFoodDetailModel>();
            foreach (var itemFood in listFoodDetail)
            {
                TableFoodDetailModel item = new TableFoodDetailModel();
                item.FoodName = db.Foods.Find(itemFood.FoodID).FoodName;
                item.FoodID = itemFood.FoodID;
                item.Quantity = itemFood.Quantity;
                item.Amount = itemFood.Amount;

                listDetail.Add(item);
            }

            return listDetail;
        }

        public TableFood InsertTable(string nameTable, int position)
        {
            TableFood tableFood = new TableFood
            {
                Status = false,
                Total = 0,
                TableName = nameTable,
                Position = position
            };

            db.TableFoods.Add(tableFood);
            db.SaveChanges();
            return tableFood;
        }

        public bool ChangeTable(int fromIDTable, int toIDTable,AccountModel account)
        {
            try
            {
                //Vào trong bảng table detail lấy hết ra các sản phẩm của bảng bị chuyển
                var dataListFoodDetail = db.TableFoodDetails.Where(x => x.TableFoodID == fromIDTable).ToList();
                //lưu ra biến tạm sau đó Chạy qua tập dữ liệu trên remove giá trị đấy
                var dataTemp = dataListFoodDetail;
                foreach (var itemFood in dataListFoodDetail)
                {
                    db.TableFoodDetails.Remove(itemFood);
                    db.SaveChanges();
                }
                //Chạy quay lai biến tạm update lại giá trị
                foreach (var itemFood in dataTemp)
                {
                    TableFoodDetail item = new TableFoodDetail
                    {
                        TableFoodID = toIDTable,
                        FoodID = itemFood.FoodID,
                        Quantity = itemFood.Quantity,
                        Amount = itemFood.Amount
                    };

                    db.TableFoodDetails.Add(item);
                    db.SaveChanges();
                }
                //Lấy ra giá trị hiện tại của table bị chuyển
                var itemTableOld = db.TableFoods.Find(fromIDTable);

                //Cập nhật lại giá trị bàn mới
                var itemTableNew = db.TableFoods.Find(toIDTable);

                itemTableNew.Status = itemTableOld.Status;
                itemTableNew.Total = itemTableOld.Total;
                db.SaveChanges();
                //Cập nhật lại giá trị của table bị chuyển về trạng thái ban đầu(bàn trống)
                itemTableOld.Status = false;
                itemTableOld.Total = 0;
                db.SaveChanges();

                //Add OrderHistory
                OrderHistory orderHistory = new OrderHistory();
                orderHistory.TableID = fromIDTable;
                orderHistory.NewTableID = toIDTable;
                orderHistory.Description = "change";
                orderHistory.CreateTime = DateTime.Now;
                orderHistory.AccountOrder = account.Username;
                orderHistory.Status = false;
                db.OrderHistories.Add(orderHistory);
                db.SaveChanges();
                //cập nhật lại giá trị bàn cũ quan bàn mới
                return true;
            }
            catch (Exception ex)
            {
                var mess = ex;
                return false;
            }
        }

        public bool GeneralTable(int fromIDTable, int toIDTable,AccountModel account)
        {
            try
            {
                var dataListFoodDetailCurrent = db.TableFoodDetails.Where(x => x.TableFoodID == fromIDTable).ToList();

                var dataListFoodDetailGeneral = db.TableFoodDetails.Where(x => x.TableFoodID == toIDTable).ToList();

                foreach (var itemDetailCurrent in dataListFoodDetailCurrent)
                {
                    bool flag = false;
                    foreach (var itemDetailGeneral in dataListFoodDetailGeneral)
                    {
                        if (itemDetailGeneral.FoodID == itemDetailCurrent.FoodID)
                        {
                            itemDetailGeneral.Quantity += itemDetailCurrent.Quantity;
                            itemDetailGeneral.Amount += itemDetailCurrent.Amount;

                            flag = true;
                            db.SaveChanges();
                        }
                        else
                            continue;
                    }

                    if (!flag)
                    {
                        TableFoodDetail tableFoodDetail = new TableFoodDetail
                        {
                            TableFoodID = toIDTable,
                            Amount = itemDetailCurrent.Amount,
                            FoodID = itemDetailCurrent.FoodID,
                            Quantity = itemDetailCurrent.Quantity
                        };

                        db.TableFoodDetails.Add(tableFoodDetail);
                        db.SaveChanges();
                    }
                }
                //Loại bỏ sản phẩm chi tiết bàn hiện tại
                foreach (var item in dataListFoodDetailCurrent)
                {
                    db.TableFoodDetails.Remove(item);
                    db.SaveChanges();
                }

                //Cập nhật lại tổng tiền
                var tableFoodCurrent = getTableByID(fromIDTable);
                var tableFoodGeneral = getTableByID(toIDTable);

                tableFoodGeneral.Total += tableFoodCurrent.Total;

                db.SaveChanges();

                //Bàn hiện tại cho về trạng thái ban đầu
                tableFoodCurrent.Status = false;
                tableFoodCurrent.Total = 0;
                db.SaveChanges();

                //Add OrderHistory
                OrderHistory orderHistory = new OrderHistory();
                orderHistory.TableID = fromIDTable;
                orderHistory.NewTableID = toIDTable;
                orderHistory.Description = "general";
                orderHistory.CreateTime = DateTime.Now;
                orderHistory.AccountOrder = account.Username;
                orderHistory.Status = false;
                db.OrderHistories.Add(orderHistory);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public object UpdateQuantityEdit(int idTable, int idFood, int quantity)
        {
            //Kiểm tra coi food còn hàng hay không
            var dataFood = new FoodDao().GetFoodByID(idFood);
            //Lấy ra tổng số sản phẩm khả dụng
            var idFoodDetail = db.TableFoodDetails.FirstOrDefault(x => x.TableFoodID == idTable && x.FoodID == idFood);
            int totalQuantity = dataFood.Quantity.Value + idFoodDetail.Quantity.Value;

            if (dataFood != null)
            {
                //Số lượng đã order bằng số lượng order mới thì không làm gì cả
                if (idFoodDetail.Quantity.Value == quantity)
                {
                    return new
                    {
                        isUpdate = 1
                    };
                }
                //nếu sl sản phẩm hiện có trên bàn này + với sl sản phẩn tồn kho > quantity update
                else if (totalQuantity >= quantity)
                {
                    idFoodDetail.Quantity = quantity;
                    idFoodDetail.Amount = quantity * dataFood.Price_Output;
                    db.SaveChanges();

                    //Update lại số lượng foood
                    var updateFood = db.Foods.Find(idFood);
                    updateFood.Quantity = totalQuantity - quantity;
                    db.SaveChanges();

                    //Update lại bàn
                    UpdateDataTableFood(idTable);

                    return new
                    {
                        isUpdate = 1
                    };
                }
                //nếu sl sản phẩm hiện có trên bàn này + với sl sản phẩn tồn kho < quantity update
                else
                {
                    return new
                    {
                        isUpdate = 0,
                        quantity = totalQuantity
                    };
                }
            }
            return new
            {
                isUpdate = 0
            };
        }

        public bool RemoveFoodDetail(int idTable, int idFood)
        {
            var itemFoodDetail = db.TableFoodDetails.FirstOrDefault(x => x.TableFoodID == idTable && x.FoodID == idFood);
            if (itemFoodDetail != null)
            {
                //Lấy ra quantity để phục hồi lại bảng sản phẩm
                int quantity = itemFoodDetail.Quantity.Value;

                db.TableFoodDetails.Remove(itemFoodDetail);
                db.SaveChanges();

                //Cập nhật lại quantity
                UpdateQuantityFood(idFood, quantity, "increase");

                ////Cập nhật lại table
                UpdateDataTableFood(idTable);

                return true;
            }
            return false;
        }

        public void UpdateQuantityFood(int idFood, int quantity, string type)
        {
            var itemFood = db.Foods.Find(idFood);
            if (itemFood != null)
            {
                if (type == "increase")
                {
                    itemFood.Quantity += quantity;
                    db.SaveChanges();
                }
                else if (type == "decrease")
                {
                    itemFood.Quantity -= quantity;
                    db.SaveChanges();
                }
            }
        }

        public void UpdateDataTableFood(int tableID)
        {
            var table = db.TableFoods.Find(tableID);

            List<TableFoodDetailModel> listDetail = GetListOrderByTable(tableID);
            if (listDetail != null)
            {
                if (table != null)
                {
                    table.Status = true;

                    //Tính tổng tiền của bàn
                    double total = 0;
                    foreach (var itemFoodDetail in listDetail)
                    {
                        total += itemFoodDetail.Amount.Value;
                    }
                    if (total == 0)
                    {
                        table.Status = false;
                    }
                    table.Total = total;
                    db.SaveChanges();
                }
            }
        }

        public object SearchTable(string statusTable, string keyword, int page = 1)
        {
            //Cấu hình page
            int pageSize = 5;
            IEnumerable<TableFood> data = db.TableFoods;

            if (statusTable == "table-empty")
            {
                data = data.Where(x => x.Status == false);
            }
            if (statusTable == "table-not-empty")
            {
                data = data.Where(x => x.Status == true);
            }
            if (keyword != string.Empty)
            {
                data = data.Where(x => x.TableName.ToNoSignFormat().ToLower().Contains(keyword.ToNoSignFormat().ToLower()));
            }

            int countItem = data.Count();
            double totalPage = Math.Ceiling((double)countItem / pageSize);

            var currenPage = page;
            if (totalPage > 0)
            {
                if (currenPage < 1)
                    currenPage = 1;

                if (currenPage > totalPage)
                    currenPage = totalPage.ToInt();
            }

            int skip = (currenPage - 1) * pageSize;
            var result = data.OrderBy(x => x.Position).Skip(skip).Take(pageSize).ToList();
            object model = new
            {
                totalPage,
                result,
                skip
            };

            return model;
        }

        public object UpdateTable(int idTable, string nameTable, int position)
        {
            var itemTable = db.TableFoods.Find(idTable);
            if (itemTable != null)
            {
                itemTable.TableName = nameTable;
                itemTable.Position = position;
                try
                {
                    db.SaveChanges();
                    return new
                    {
                        status = 1,
                        message = "Bạn đã thêm bàn thành công"
                    };
                }
                catch
                {
                    return new
                    {
                        status = 0,
                        message = "Lỗi hệ thống! Vui lòng thử lại"
                    };
                }
            }

            return new
            {
                status = 0,
                message = "Bàn không được tìm thấy! Vui lòng thử lại"
            };
        }

        public object RemoveTable(int idTable)
        {
            var itemTable = db.TableFoods.Find(idTable);
            if (itemTable != null)
            {
                db.TableFoods.Remove(itemTable);

                try
                {
                    db.SaveChanges();
                    return new
                    {
                        status = 1,
                        message = "Bạn đã xóa bàn thành công"
                    };
                }
                catch
                {
                    return new
                    {
                        status = 0,
                        message = "Lỗi hệ thống! Vui lòng thử lại"
                    };
                }
            }

            return new
            {
                status = 0,
                message = "Bàn không được tìm thấy! Vui lòng thử lại"
            };
        }

    }
}
﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class FoodTypeDao
    {
        DBContext db = null;
        public FoodTypeDao()
        {
            db = new DBContext();
        }

        public FoodType GetFoodTypeByID(int id)
        {
            return db.FoodTypes.Find(id);
        }

        public List<FoodType> GetAllFoodType()
        {
            return db.FoodTypes.Where(x => x.Status == true).OrderByDescending(x => x.CreateTime).OrderBy(x=>x.Position).ToList();
        }

        public bool InsertFoodType(string name,int position)
        {
            FoodType foodType = new FoodType();
            foodType.Name = name;
            foodType.Position = position;
            foodType.Status = true;
            foodType.CreateTime = DateTime.Now;

            try
            {
                db.FoodTypes.Add(foodType);
                db.SaveChanges();
                return true;
            }
            catch
            {

                return false ;
            }

        }

        public bool RemoveFoodType(int idFoodType)
        {
            var itemFoodType = GetFoodTypeByID(idFoodType);

            if (itemFoodType != null)
            {
                db.FoodTypes.Remove(itemFoodType);
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool UpdateFoodType(int idFoodType,string name,int position)
        {
            var itemFoodType = db.FoodTypes.Find(idFoodType);

            if (itemFoodType != null)
            {
                itemFoodType.Name = name;
                itemFoodType.CreateTime = DateTime.Now;
                itemFoodType.Position = position;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }
    }
}

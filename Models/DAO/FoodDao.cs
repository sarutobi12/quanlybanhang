﻿using CodeUtility;
using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Models.DAO
{
    public class FoodDao
    {
        private DBContext db = null;

        public FoodDao()
        {
            db = new DBContext();
        }

        public object SearchFood(int foodTypeID, string keyword, int page)
        {
            //Cấu hình page
            int pageSize = 5;

            var data = db.Foods.Join(db.FoodTypes, f => f.FoodTypeID, t => t.FoodTypeID, (f, t) => new FoodModel
            {
                FoodID = f.FoodID,
                FoodName = f.FoodName,
                Price_Import = f.Price_Import,
                Price_Output = f.Price_Output,
                CreateTime = f.CreateTime,
                CreateBy = f.CreateBy,
                Status = f.Status,
                Quantity = f.Quantity,
                FoodTypeID = f.FoodTypeID,
                Description = f.Description,
                Position = f.Position,
                FoodTypeName = t.Name
            });

            if (foodTypeID != 0)
            {
                data = data.Where(x => x.FoodTypeID == foodTypeID);
            }
            if (!keyword.IsNullOrEmpty())
            {
                data = data.Where(x => x.FoodName.Contains(keyword));
            }

            int countItem = data.Count();
            double totalPage = Math.Ceiling((double)countItem / pageSize);

            var currenPage = page;
            if (totalPage > 0)
            {
                if (currenPage < 1)
                    currenPage = 1;

                if (currenPage > totalPage)
                    currenPage = totalPage.ToInt();
            }

            int skip = (currenPage - 1) * pageSize;
            var result = data.OrderByDescending(x => x.CreateTime).Skip(skip).Take(pageSize).ToList();
            object model = new
            {
                totalPage,
                result,
                skip
            };

            return model;
        }

        public List<FoodModel> GetAllFood()
        {
            var data = db.Foods.Join(db.FoodTypes, f => f.FoodTypeID, t => t.FoodTypeID, (f, t) => new FoodModel
            {
                FoodID = f.FoodID,
                FoodName = f.FoodName,
                Price_Import = f.Price_Import,
                Price_Output = f.Price_Output,
                CreateTime = f.CreateTime,
                CreateBy = f.CreateBy,
                Status = f.Status,
                Quantity = f.Quantity,
                FoodTypeID = f.FoodTypeID,
                Description = f.Description,
                Position = f.Position,
                FoodTypeName = t.Name
            }).OrderByDescending(x => x.CreateTime).ToList();
            return data;
        }

        public FoodModel GetFoodByID(int idFood)
        {
            var data = db.Foods.Join(db.FoodTypes, f => f.FoodTypeID, t => t.FoodTypeID, (f, t) => new FoodModel
            {
                FoodID = f.FoodID,
                FoodName = f.FoodName,
                Price_Import = f.Price_Import,
                Price_Output = f.Price_Output,
                CreateTime = f.CreateTime,
                CreateBy = f.CreateBy,
                Status = f.Status,
                Quantity = f.Quantity,
                FoodTypeID = f.FoodTypeID,
                Description = f.Description,
                Position = f.Position,
                FoodTypeName = t.Name
            }).FirstOrDefault(x => x.FoodID == idFood);

            return data;
        }

        public List<FoodModel> GetFoodByType(int id)
        {
            var data = db.Foods.Join(db.FoodTypes, f => f.FoodTypeID, t => t.FoodTypeID, (f, t) => new FoodModel
            {
                FoodID = f.FoodID,
                FoodName = f.FoodName,
                Price_Import = f.Price_Import,
                Price_Output = f.Price_Output,
                CreateTime = f.CreateTime,
                CreateBy = f.CreateBy,
                Status = f.Status,
                Quantity = f.Quantity,
                FoodTypeID = f.FoodTypeID,
                Description = f.Description,
                Position = f.Position,
                FoodTypeName = t.Name
            }).Where(x => x.FoodTypeID == id);

            return data.ToList();
        }

        public double? GetPriceByIDFood(int idFood)
        {
            return db.Foods.Find(idFood).Price_Output;
        }

        public List<Food> Search(string keyword, int page = 1)
        {
            var data = db.Foods.Where(x => x.Status == true);
            if (keyword != string.Empty)
            {
                //data= data.Where(x=>x.)
            }
            return db.Foods.Where(x => x.Status == true && x.FoodName.Contains(keyword)).ToList();
        }

        public bool InsertFood(string foodName, int idFoodType, string description, int priceImport, int priceOutput)
        {
            Food food = new Food();
            food.FoodName = foodName;
            food.FoodTypeID = idFoodType;
            food.Description = description;
            food.Price_Import = priceImport;
            food.Price_Output = priceOutput;
            food.CreateTime = DateTime.Now;
            food.Quantity = 0;
            food.CreateBy = "admin";
            food.Status = true;

            try
            {
                db.Foods.Add(food);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateFood(int idFood, string foodName, int idFoodType, string description, int priceImport, int priceOutput)
        {
            var itemFood = db.Foods.Find(idFood);
            if (itemFood != null)
            {
                itemFood.FoodName = foodName;
                itemFood.Description = description;
                itemFood.FoodTypeID = idFoodType;
                itemFood.Price_Import = priceImport;
                itemFood.Price_Output = priceOutput;

                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public bool RemoveFood(int idFood)
        {
            var itemFood = db.Foods.Find(idFood);
            if (itemFood != null)
            {
                db.Foods.Remove(itemFood);
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        public bool UpdateQuantity(int idFood, int quantity)
        {
            var itemFood = db.Foods.Find(idFood);
            if (itemFood != null)
            {
                itemFood.Quantity = quantity;
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
    }
}
﻿using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class PaymentDao
    {
        DBContext db = null;

        public PaymentDao()
        {
            db = new DBContext();
        }

        public object Payment(int idTable, int bonus,AccountModel accountOrder)
        {
            try
            {
                //Lấy chi tiết bàn ăn hiện tại
                var itemTableCurrent = db.TableFoods.Find(idTable);
                //Lấy ra danh sách món trong bàn ăn hiện tại
                var listDataFoodDetailCurrent = db.TableFoodDetails.Where(x => x.TableFoodID == idTable).ToList();
                //Gán chi tiết bàn hiền tại vào bill
                Bill bill = new Bill();
                bill.Total = itemTableCurrent.Total;
                bill.TotalAmount = (itemTableCurrent.Total - (itemTableCurrent.Total * bonus) / 100);
                bill.CreateTime = DateTime.Now;
                bill.Bonus = bonus;
                bill.TableFoodID = idTable;
                bill.Status = true;
                bill.CreateBy = accountOrder.Username;
                db.Bills.Add(bill);
                db.SaveChanges();

                //Update dữ liệu trên bàn ăn hiện tại

                itemTableCurrent.Status = false;
                itemTableCurrent.Total = 0;
                db.SaveChanges();

                //Gán danh sách món trong bàn ăn hiện tại vào order
                foreach (var itemFoodDetail in listDataFoodDetailCurrent)
                {
                    BillDetail billDetail = new BillDetail();
                    billDetail.BillID = bill.BillID;
                    billDetail.Amount = itemFoodDetail.Amount;
                    billDetail.FoodID = itemFoodDetail.FoodID;
                    billDetail.Quantity = itemFoodDetail.Quantity;

                    db.BillDetails.Add(billDetail);
                    db.SaveChanges();

                    //Remove chi tiết trên bàn
                    db.TableFoodDetails.Remove(itemFoodDetail);
                    db.SaveChanges();
                }

                //Add OrderHistory
                OrderHistory orderHistory = new OrderHistory();
                orderHistory.TableID = idTable;
                orderHistory.Description = "payment";
                orderHistory.CreateTime = DateTime.Now;
                orderHistory.AccountOrder = accountOrder.Username;
                orderHistory.Status = false;
                db.OrderHistories.Add(orderHistory);
                db.SaveChanges();
                return new {
                    isPayment =true,
                    BillID = bill.BillID
                };
            }
            catch
            {
                return new
                {
                    isPayment = false
                };
            }

        }
    }
}

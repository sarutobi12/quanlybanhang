﻿using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class MenuDao
    {
        DBContext db = null;
        public MenuDao()
        {
            db = new DBContext();
        }
        public IEnumerable<MenuModel> GetAllMenu(AccountModel sessAccount)
        {
            var dataMenu = db.MenuAccountMains.Where(x=>x.AccountMainID ==sessAccount.AccountMainID)
                .Join(db.Menus,m =>m.MenuID , mn =>mn.ID,(m,mn)=> new MenuModel{
                    ID = mn.ID,
                    Icon =  mn.Icon,
                    Name = mn.Name,
                    Path = mn.Path,
                    Role= mn.Role,
                    Status =mn.Status,
                    Title = mn.Title
                });
            var data = dataMenu.ToList();
            return data;

        }
        public object GetMenuByAccountID(int id)
        {
            var dataAllMenu = db.Menus.Select(x => new MenuCheckboxModel
            {
                ID = x.ID,
                Title = x.Title
            });

            var dataMenuAccount = db.MenuAccountMains.Where(x => x.AccountMainID == id).ToList();

            List<MenuCheckboxModel> menuCheckboxModels = new List<MenuCheckboxModel>();
            foreach (var itemAllMenu in dataAllMenu)
            {
                MenuCheckboxModel item = new MenuCheckboxModel();
                item.ID = itemAllMenu.ID;
                item.Title = itemAllMenu.Title;
                foreach (var itemMenuAccount in dataMenuAccount)
                {
                    if (itemMenuAccount.MenuID == itemAllMenu.ID)
                    {
                        item.isCheck = true;
                        break;
                    }
                }
                menuCheckboxModels.Add(item);
            }
            return menuCheckboxModels;

        }

        public bool UpdateMenuAccountMain(int accountMainID, List<MenuCheckboxModel> listMenu, AccountModel sessionAccount)
        {
            try
            {
                var dataAccountMain = db.MenuAccountMains.Where(x => x.AccountMainID == accountMainID).ToList();

                db.MenuAccountMains.RemoveRange(dataAccountMain);
                db.SaveChanges();
                if (listMenu!=null)
                {
                    foreach (var itemMenu in listMenu)
                    {
                        MenuAccountMain menu = new MenuAccountMain();
                        menu.AccountMainID = accountMainID;
                        menu.MenuID = itemMenu.ID;
                        menu.UpdateBy = sessionAccount.Username;
                        menu.UpdateTime = DateTime.Now;

                        db.MenuAccountMains.Add(menu);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }


        }

    }
}

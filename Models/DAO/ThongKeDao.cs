﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class ThongKeDao
    {
        DBContext db = null;

        public ThongKeDao()
        {
            db = new DBContext();
        }

        public int GetCountFood()
        {
            return db.Foods.Count();
        }
        public int GetQuantityFood()
        {
            var data = db.Foods.ToList();

            int quantity = 0;
            foreach (var item in data)
            {
                quantity += item.Quantity.Value;
            }

            return quantity;
        }
        public object GetQuantityNearFood()
        {
            var data = db.Foods.Where(x => x.Quantity < 10 && x.Quantity > 0).ToList(); ;

            int countFood = data.Count;
            //Sắp hết hàng là những sản phẩm có số lượng dưới 10

            return new { countFood, result= data };
        }
        public object GetCountFoodOutOfStock()
        {
            var data = db.Foods.Where(x=>x.Quantity==0).ToList();

            int countFood = data.Count;
            // hết hàng là những sản phẩm có số lượng =0
            return new { countFood, result = data };
        }
    }
}

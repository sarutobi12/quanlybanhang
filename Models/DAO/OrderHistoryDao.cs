﻿using CodeUtility;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class OrderHistoryDao
    {
        DBContext db = null;
        public OrderHistoryDao()
        {
            db = new DBContext();
        }

        public object GetDataHistory()
        {
            var data = from h in db.OrderHistories
                       join f in db.Foods on h.FoodID equals f.FoodID into food
                       join a in db.Accounts on h.AccountOrder equals a.Username into account
                       from fd in food.DefaultIfEmpty()
                       from ac in account.DefaultIfEmpty()
                       select new
                       {
                           ID = h.ID,
                           FoodID = h.FoodID,
                           TableID = h.TableID,
                           NewTableID =h.NewTableID,
                           NewQuantity = h.NewQuantity,
                           OldQuantity = h.OldQuantity,
                           AccountOrder = h.AccountOrder,
                           Description = h.Description,
                           CreateTime = h.CreateTime,
                           FoodName = fd.FoodName,
                           FullName = ac.FullName,
                           Status =h.Status,

                       };

            int coutNewHistory = data.Count(x => x.Status == false);

            var result = new
            {
                result = data.OrderByDescending(x => x.CreateTime).Take(10),
                coutNewHistory
            };

            return result;
        }

        public object SearchHistory(int page)
        {
            int pageSize = 10;

            var data = from h in db.OrderHistories
                       join f in db.Foods on h.FoodID equals f.FoodID into food
                       join a in db.Accounts on h.AccountOrder equals a.Username into account
                       from fd in food.DefaultIfEmpty()
                       from ac in account.DefaultIfEmpty()
                       select new
                       {
                           ID = h.ID,
                           FoodID = h.FoodID,
                           TableID = h.TableID,
                           NewTableID = h.NewTableID,
                           NewQuantity = h.NewQuantity,
                           OldQuantity = h.OldQuantity,
                           AccountOrder = h.AccountOrder,
                           Description = h.Description,
                           CreateTime = h.CreateTime,
                           FoodName = fd.FoodName,
                           FullName = ac.FullName,
                           Status = h.Status,
                       };

            int countItem = data.Count();
            double totalPage = Math.Ceiling((double)countItem / pageSize);

            var currenPage = page;
            if (totalPage > 0)
            {
                if (currenPage < 1)
                    currenPage = 1;

                if (currenPage > totalPage)
                    currenPage = totalPage.ToInt();
            }

            int skip = (currenPage - 1) * pageSize;
            var result = data.OrderByDescending(x => x.CreateTime).Skip(skip).Take(pageSize).ToList();

            object model = new
            {
                totalPage,
                result,
                skip
            };

            return model;
        }
    }
}

﻿using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeUtility;
using System.Data.Entity;
using Models.Util;

namespace Models.DAO
{
    public class BillDao
    {
        DBContext db = null;
        public BillDao()
        {
            db = new DBContext();
        }
        public int GetCountBill()
        {
            return db.Bills.ToList().Count;
        }
        public object SearchBill(int tableID, string keyword, string fromTime, string toTime, int page = 1)
        {
            //Cấu hình page
            int pageSize = 5;
            var data = from b in db.Bills
                       join a in db.Accounts on b.CreateBy equals a.Username into account
                       join t in db.TableFoods on b.TableFoodID equals t.TableID into table
                       from ac in account.DefaultIfEmpty()
                       from tb in table.DefaultIfEmpty()
                       select new
                       {
                           BillID = b.BillID,
                           CreateTime = b.CreateTime,
                           Total = b.Total,
                           TotalAmount = b.TotalAmount,
                           Bonus = b.Bonus,
                           TableID = b.TableFoodID,
                           TableName = tb.TableName,
                           FullName = ac.FullName
                       };

            if (fromTime != string.Empty && toTime != string.Empty)
            {
                DateTime fromDateConvert = Convert.ToDateTime(fromTime).Date;
                DateTime endDateConvert = Convert.ToDateTime(toTime).Date;
                data = data.Where(x => DbFunctions.TruncateTime(x.CreateTime) >= fromDateConvert
                && DbFunctions.TruncateTime(x.CreateTime) <= endDateConvert);
            }
            if (tableID != 0)
            {
                data = data.Where(x => x.TableID.ToString().Contains(tableID.ToString()));
            }
            if (keyword != string.Empty)
            {
                data = data.Where(x => x.TableName.Contains(keyword)
                || x.FullName.Contains(keyword)
                );
            }

            int countItem = data.Count();
            double totalPage = Math.Ceiling((double)countItem / pageSize);

            var currenPage = page;
            if (totalPage > 0)
            {
                if (currenPage < 1)
                    currenPage = 1;

                if (currenPage > totalPage)
                    currenPage = totalPage.ToInt();
            }

            int skip = (currenPage - 1) * pageSize;
            var result = data.OrderByDescending(x => x.BillID).Skip(skip).Take(pageSize).ToList();
            object model = new
            {
                totalPage,
                result,
                skip
            };

            return model;

        }

        public Bill GetBillByID(int id)
        {
            return db.Bills.Find(id);
        }

        public object GetBillDetailByBillID(int billID)
        {
            var data = from b in db.BillDetails
                       join f in db.Foods on b.FoodID equals f.FoodID into billDetail
                       from fd in billDetail.DefaultIfEmpty()
                       where b.BillID == billID
                       select new
                       {
                           BillID = b.BillID,
                           FoodName = fd.FoodName,
                           Quantity = b.Quantity,
                           Amount = b.Amount
                       };
            return data.ToList();
        }

        public BillPrintModel GetDetailBill(int billID)
        {
            var bill = GetBillByID(billID);

            var data = db.BillDetails.Where(x => x.BillID == billID).ToList();

            List<BillDetailPrintModel> billDetail = new List<BillDetailPrintModel>();
            foreach (var item in data)
            {
                BillDetailPrintModel itemDetail = new BillDetailPrintModel();
                itemDetail.FoodName             = new FoodDao().GetFoodByID(item.FoodID).FoodName;
                itemDetail.Amount               = item.Amount;
                itemDetail.Price                = new FoodDao().GetFoodByID(item.FoodID).Price_Output;
                itemDetail.Quantity             = item.Quantity;

                billDetail.Add(itemDetail);
            }

            BillPrintModel printModel = new BillPrintModel();
            printModel.BillID         = bill.BillID;
            printModel.Bonus          = bill.Bonus;
            printModel.CreateTime     = bill.CreateTime;
            printModel.TableFoodID    = bill.TableFoodID;
            printModel.Total          = bill.Total;
            printModel.TotalAmount    = bill.TotalAmount;
            printModel.BillDetail     = billDetail;


            return printModel;

        }

        public AccountModel PrintCardEmploy(string username)
        {
            AccountModel data = db.Accounts.Join(db.AccountMains, a => a.AccountMainID, m => m.AccountMainID, (a, m) => new AccountModel
            {
                Username         = a.Username,
                Password         = a.Password,
                FullName         = a.FullName,
                Address          = a.Address,
                Avatar           = a.Avatar,
                BirthDay         = a.BirthDay,
                CreateTime       = a.CreateTime,
                Description      = a.Description,
                Email            = a.Email,
                Mobi             = a.Mobi,
                Position         = a.Position,
                Status           = a.Status,
                QRCodeHass       = a.QRCodeHass,
                AccountMainID    = a.AccountMainID.Value,
                AccountMainTitle = m.Title

            }).FirstOrDefault(x => x.Username == username);
            return data;
        }
    }
}

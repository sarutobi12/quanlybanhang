using System.Web.Mvc;
using System.Web.Routing;

namespace QuanLyBanHang
{
  public class RouteConfig
  {
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.MapRoute(
       name: "GetTableByID",
       url: "{controller}/{action}/{idTable}",
       defaults: new
       {
         controller = "TableFood",
         action = "getTableByID"
       }
      );
      routes.MapRoute(
       name: "GetAllFood",
       url: "get-all-food",
       defaults: new
       {
         controller = "Food",
         action = "GetAllFood"
       }
      );

      routes.MapRoute(
       name: "GetAllFoodByType",
       url: "food-by-type/{idFoodType}",
       defaults: new
       {
         controller = "Food",
         action = "GetFoodByType",
         idFoodType = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "OrderFood",
       url: "order-food",
       defaults: new
       {
         controller = "TableFood",
         action = "Order",
         tableID = UrlParameter.Optional,
         idFood = UrlParameter.Optional,
         description = UrlParameter.Optional,
         quantity = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "OrderDetail",
       url: "order-detail/{idTable}",
       defaults: new
       {
         controller = "TableFood",
         action = "GetListOrderByTable"
       }
      );
      routes.MapRoute(
       name: "RemoveFoodDetail",
       url: "xoa-order",
       defaults: new
       {
         controller = "TableFood",
         action = "RemoveFoodDetail",
         idTable = UrlParameter.Optional,
         idFood = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "UpdateQuantityFoodDetail",
       url: "update-quantity-food-detail",
       defaults: new
       {
         controller = "TableFood",
         action = "UpdateQuantityDetail",
         idTable = UrlParameter.Optional,
         idFood = UrlParameter.Optional,
         quantity = UrlParameter.Optional
       }
      );
      routes.MapRoute(
       name: "ChangeTable",
       url: "change-table",
       defaults: new
       {
         controller = "TableFood",
         action = "ChangeTable",
         fromIDTable = UrlParameter.Optional,
         toIDTable = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "InsertTable",
       url: "insert-table",
       defaults: new
       {
         controller = "TableFood",
         action = "InserTable",
         tableName = UrlParameter.Optional,
         position = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "Update-table",
       url: "update-table",
       defaults: new
       {
         controller = "TableFood",
         action = "UpdateTable",
         idTable = UrlParameter.Optional,
         tableName = UrlParameter.Optional,
         position = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "remove-table",
       url: "remove-table",
       defaults: new
       {
         controller = "TableFood",
         action = "RemoveTable",
         idTable = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "GeneralTable",
       url: "general-table",
       defaults: new
       {
         controller = "TableFood",
         action = "GeneralTable",
         fromIDTable = UrlParameter.Optional,
         toIDTable = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "payment",
       url: "payment",
       defaults: new
       {
         controller = "Payment",
         action = "Payment",
         idTable = UrlParameter.Optional,
         bonus = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "print-bill",
       url: "print-bill/{ID}",
       defaults: new
       {
         controller = "PrintBill",
         action = "Index"
       }
      );

      routes.MapRoute(
       name: "insert-food-type",
       url: "insert-food-type",
       defaults: new
       {
         controller = "FoodType",
         action = "InsertFoodType",
         name = UrlParameter.Optional,
         position = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "update-food-type",
       url: "update-food-type",
       defaults: new
       {
         controller = "FoodType",
         action = "UpdateFoodType",
         idFoodType = UrlParameter.Optional,
         name = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "remove-food-type",
       url: "remove-food-type/{idFoodType}",
       defaults: new
       {
         controller = "FoodType",
         action = "RemoveFoodType"
       }
      );

      routes.MapRoute(
       name: "insert-food",
       url: "insert-food",
       defaults: new
       {
         controller = "Food",
         action = "InsertFood",
         foodName = UrlParameter.Optional,
         idFoodType = UrlParameter.Optional,
         description = UrlParameter.Optional,
         priceImport = UrlParameter.Optional,
         priceOuput = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "update-food",
       url: "update-food",
       defaults: new
       {
         controller = "Food",
         action = "UpdateFood",
         idFood = UrlParameter.Optional,
         foodName = UrlParameter.Optional,
         idFoodType = UrlParameter.Optional,
         description = UrlParameter.Optional,
         priceImport = UrlParameter.Optional,
         priceOuput = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "remove-food",
       url: "remove-food/{idFood}",
       defaults: new
       {
         controller = "Food",
         action = "RemoveFood"
       }
      );
      routes.MapRoute(
       name: "search-food",
       url: "search-food",
       defaults: new
       {
         controller = "Food",
         action = "SearchFood",
         foodTypeID = UrlParameter.Optional,
         keyword = UrlParameter.Optional,
         page = UrlParameter.Optional
       }
      );
      routes.MapRoute(
       name: "import-session-food",
       url: "import-session-food",
       defaults: new
       {
         controller = "ImportFood",
         action = "AddImportToSession",
         idFood = UrlParameter.Optional,
         quantityImport = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "remove-session-food",
       url: "remove-session-food/{id}",
       defaults: new
       {
         controller = "ImportFood",
         action = "RemoveCartItem"
       }
      );
      routes.MapRoute(
       name: "insert-account-main",
       url: "insert-account-main",
       defaults: new
       {
         controller = "AccountMain",
         action = "InsertAccountMain",
         title = UrlParameter.Optional,
         position = UrlParameter.Optional
       }
      );

      routes.MapRoute(
       name: "update-account-main",
       url: "update-account-main",
       defaults: new
       {
         controller = "AccountMain",
         action = "UpdateAccountMain",
         accountMainID = UrlParameter.Optional,
         title = UrlParameter.Optional,
         position = UrlParameter.Optional
       }
      );

      routes.MapRoute(
      name: "remove-account-main",
      url: "remove-account-main",
      defaults: new
      {
        controller = "AccountMain",
        action = "RemoveAccountMain",
        accountMainID = UrlParameter.Optional
      }
     );

      routes.MapRoute(
     name: "insert-account",
     url: "insert-account",
     defaults: new
     {
       controller = "Account",
       action = "InsertAccount",
       username = UrlParameter.Optional,
       password = UrlParameter.Optional,
       accountMainID = UrlParameter.Optional,
       fullName = UrlParameter.Optional,
       address = UrlParameter.Optional,
       mobi = UrlParameter.Optional,
       email = UrlParameter.Optional,
       status = UrlParameter.Optional,
     }
    );

      routes.MapRoute(
   name: "update-account",
   url: "update-account",
   defaults: new
   {
     controller = "Account",
     action = "UpdateAccount",
     username = UrlParameter.Optional,
     password = UrlParameter.Optional,
     accountMainID = UrlParameter.Optional,
     fullName = UrlParameter.Optional,
     address = UrlParameter.Optional,
     mobi = UrlParameter.Optional,
     email = UrlParameter.Optional,
     status = UrlParameter.Optional,
   });

      routes.MapRoute(
 name: "remove-account",
 url: "remove-account",
 defaults: new
 {
   controller = "Account",
   action = "RemoveAccount",
   username = UrlParameter.Optional
 }
);

      routes.MapRoute(
 name: "search-account",
 url: "search-account",
 defaults: new
 {
   controller = "Account",
   action = "SearchAccount",
   accountMainID = UrlParameter.Optional,
   keyword = UrlParameter.Optional,
   page = UrlParameter.Optional,
 }
);

      routes.MapRoute(
name: "print-card",
url: "print-card/{username}",
defaults: new
{
  controller = "PrintBill",
  action = "PrintCardEmploy"
}
);

      routes.MapRoute(
name: "import-history",
url: "import-history",
defaults: new
{
  controller = "ImportFood",
  action = "ImportHistory",
  page = UrlParameter.Optional,
  fromDate = UrlParameter.Optional,
  endDate = UrlParameter.Optional
}
);

      routes.MapRoute(
name: "get-import-detail",
url: "get-import-detail/{idImportFood}",
defaults: new
{
  controller = "ImportFood",
  action = "GetImportDetailByID"
}
);

      routes.MapRoute(
name: "get-menu-by-id",
url: "get-menu-by-id/{id}",
defaults: new
{
  controller = "Menu",
  action = "GetMenuByAccountID"
}
);
      routes.MapRoute(
name: "search-bill",
url: "search-bill",
defaults: new
{
  controller = "Bill",
  action = "SearchBill",
  tableID = UrlParameter.Optional,
  keyword = UrlParameter.Optional,
  fromTime = UrlParameter.Optional,
  toTime = UrlParameter.Optional,
  page = UrlParameter.Optional
}
);

      routes.MapRoute(
name: "get-bill-detail",
url: "get-bill-detail/{billID}",
defaults: new
{
controller = "Bill",
action = "GetBillDetailByBillID"
}
);

      routes.MapRoute(
name: "search-noti",
url: "search-noti/{page}",
defaults: new
{
controller = "OrderHistory",
action = "SearchHistory"
}
);


      routes.MapRoute(
       name: "Default",
       url: "{controller}/{action}/{id}",
       defaults: new
       {
         controller = "Home",
         action = "Index",
         id = UrlParameter.Optional
       }
      );
    }
  }
}

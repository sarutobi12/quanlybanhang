using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyBanHang.Models
{
  public class Cart
  {

    public Dictionary<int, CartItem> cartItems;
    public Dictionary<int, CartItem> CartItems
    {
      get
      {
        if (cartItems == null)
          cartItems = new Dictionary<int, CartItem>();

        return cartItems;
      }
    }

    public double Amount
    {
      get
      {
        double total = 0;
        foreach (var item in CartItems)
        {
          total += item.Value.Total;
        }
        return total;
      }
    }
  }
}

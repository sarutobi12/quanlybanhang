using Models.DAO;
using QuanLyBanHang.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class PaymentController : Controller
  {
    // GET: Payment
    public ActionResult Index()
    {
      return View();
    }


    [HttpPost]
    public JsonResult Payment(int idTable, int bonus)
    {
      return Json(new PaymentDao().Payment(idTable, bonus,SessionUtility.Account),JsonRequestBehavior.AllowGet);
    }
  }
}

using Models.DAO;
using QuanLyBanHang.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeUtility;
using Models.Models;
using Models.ViewModel;

namespace QuanLyBanHang.Controllers
{
  public class LoginController : Controller
  {
    // GET: Login
    public ActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public JsonResult Login(string username, string password)
    {

      string hassPassword = password.SHA256Hash();
      int isLogin = new AccountDao().Login(username, hassPassword);
      if (isLogin == 2)
      {
        SessionUtility.Account = new AccountDao().GetAccountByID(username);
        string url = new AccountDao().GetDefaultUrlAccount(SessionUtility.Account.AccountMainID);
        return Json(new { isLogin, url }, JsonRequestBehavior.AllowGet);
      }
      else
        return Json(new { isLogin }, JsonRequestBehavior.AllowGet);
    }
    [HttpPost]
    public JsonResult LoginByQrCode(string hass)
    {

      LoginModal isLogin = new AccountDao().LoginByQrCode(hass);
      if (isLogin.Result == 1)
      {
        SessionUtility.Account = isLogin.Account;
        string url = new AccountDao().GetDefaultUrlAccount(SessionUtility.Account.AccountMainID);
        return Json(new { isLogin, url }, JsonRequestBehavior.AllowGet);
      }

      return Json(new { isLogin }, JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetAccountLogin()
    {
      if (SessionUtility.Account != null)
      {

        return Json(new { Account = SessionUtility.Account, isSessionLogin = true }, JsonRequestBehavior.AllowGet);
      }
      else
      {
        return Json(new { isSessionLogin = false }, JsonRequestBehavior.AllowGet);
      }
    }
  }
}

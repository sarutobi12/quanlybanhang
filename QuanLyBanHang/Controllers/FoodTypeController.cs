using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class FoodTypeController : Controller
  {
    // GET: FoodType
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult GetAllFoodType()
    {
      return Json(new FoodTypeDao().GetAllFoodType(), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult InsertFoodType(string name,int position)
    {
      return Json(new FoodTypeDao().InsertFoodType(name,position), JsonRequestBehavior.AllowGet);
    }

    public JsonResult RemoveFoodType(int idFoodType)
    {
      return Json(new FoodTypeDao().RemoveFoodType(idFoodType), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult UpdateFoodType(int idFoodType,string name,int position)
    {
      return Json(new FoodTypeDao().UpdateFoodType(idFoodType,name, position), JsonRequestBehavior.AllowGet);
    }
  }
}

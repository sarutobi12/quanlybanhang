using Models.DAO;
using Models.ViewModel;
using QuanLyBanHang.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class ImportFoodController : Controller
  {
    // GET: ImportFood
    public ActionResult Index()
    {
      return View();
    }
    [HttpPost]
    public JsonResult AddImportToSession(int idFood, int quantityImport)
    {
      var itemFood = new FoodDao().GetFoodByID(idFood);

      if (itemFood == null)
      {
        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
      }

      CartItem cartItem;
      if (!SessionUtility.Cart.CartItems.ContainsKey(idFood))
      {
        cartItem = new CartItem();

        cartItem.ID = idFood;
        cartItem.FoodName = itemFood.FoodName;
        cartItem.OldQuantity = itemFood.Quantity.Value;
        cartItem.NewQuantity = quantityImport;
        cartItem.PriceImport = itemFood.Price_Import.Value;

        SessionUtility.Cart.CartItems.Add(idFood, cartItem);
      }
      else
      {
        cartItem = SessionUtility.Cart.CartItems[idFood];
        cartItem.NewQuantity += quantityImport;
      }

      return Json(new { result = true }, JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetAllSessionCart()
    {
      var data = SessionUtility.Cart == null ? new Cart() : SessionUtility.Cart;

      List<CartItem> listCart = new List<CartItem>();

      foreach (var item in SessionUtility.Cart.CartItems)
      {
        CartItem cartItem = new CartItem();
        cartItem.ID = item.Value.ID;
        cartItem.FoodName = item.Value.FoodName;
        cartItem.Description = item.Value.Description;
        cartItem.NewQuantity = item.Value.NewQuantity;
        cartItem.OldQuantity = item.Value.OldQuantity;
        cartItem.PriceImport = item.Value.PriceImport;

        listCart.Add(cartItem);
      }
      object result = new
      {
        Amount = SessionUtility.Cart.Amount,
        result = listCart
      };
      return Json(result, JsonRequestBehavior.AllowGet);
    }

    public JsonResult RemoveCartItem(int id)
    {
      if (SessionUtility.Cart != null && SessionUtility.Cart.CartItems.ContainsKey(id))
      {
        SessionUtility.Cart.CartItems.Remove(id);
        return Json(new { result = true }, JsonRequestBehavior.AllowGet);
      }
      else
      {
        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
      }


    }

    public JsonResult AddImport()
    {
      var dataSession = SessionUtility.Cart.CartItems;
      foreach (var itemFood in dataSession)
      {
        new FoodDao().UpdateQuantity(itemFood.Value.ID, itemFood.Value.TotalQuantity);
      }

      //Lưu vào bảng lịch sử nhập hàng
      var result = new ImportDao().AddImport(SessionUtility.Cart,SessionUtility.Account);

      SessionUtility.Cart.CartItems.Clear();

      return Json(new { result }, JsonRequestBehavior.AllowGet);

    }

    [HttpPost]
    public JsonResult ImportHistory(string fromDate, string endDate, int page = 1)
    {
      return Json(new ImportDao().ImportHistory(fromDate,endDate,page),JsonRequestBehavior.AllowGet);
    }
    [HttpGet]
    public JsonResult GetImportDetailByID(int idImportFood)
    {
      return Json(new ImportDao().GetImportDetailByID(idImportFood), JsonRequestBehavior.AllowGet);
    }
  }
}

using Models.DAO;
using Models.ViewModel;
using QuanLyBanHang.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class MenuController : Controller
  {
    // GET: Menu
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult GetAllMenu()
    {
      return Json(new MenuDao().GetAllMenu(SessionUtility.Account), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public JsonResult GetMenuByAccountID(int id)
    {
      return Json(new MenuDao().GetMenuByAccountID(id), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult UpdateMenuAccountMain(int accountMainID, List<MenuCheckboxModel> listMenu)
    {
      return Json(new MenuDao().UpdateMenuAccountMain(accountMainID, listMenu, SessionUtility.Account), JsonRequestBehavior.AllowGet);
    }
  }
}

using Models.DAO;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class AccountMainController : Controller
  {
    // GET: AccountMain
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult GetAllAccountMains()
    {
      return Json(new AccountMainDao().GetAllAccountMains(), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult InsertAccountMain(string title, int position)
    {
      return Json(new AccountMainDao().InsertAccountMain(title, position));
    }

    [HttpPost]
    public JsonResult UpdateAccountMain(int accountMainID, string title, int position)
    {
      return Json(new AccountMainDao().UpdateAccountMain(accountMainID, title, position));
    }

    [HttpPost]
    public JsonResult RemoveAccountMain(int accountMainID)
    {
      return Json(new AccountMainDao().RemoveAccountMain(accountMainID));
    }
  }
}

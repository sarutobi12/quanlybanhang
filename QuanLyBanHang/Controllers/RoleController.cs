using Models.DAO;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class RoleController : Controller
  {
    // GET: Role
    public ActionResult Index()
    {
      return View();
    }
    [HttpGet]
    public JsonResult GetAllRole()
    {
      return Json(new RoleDao().GetAllRole(),JsonRequestBehavior.AllowGet);
    }

   
  }
}

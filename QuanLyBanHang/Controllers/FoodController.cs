using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodeUtility;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class FoodController : Controller
  {
    // GET: Food
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult GetAllFood()
    {
      return Json(new FoodDao().GetAllFood(), JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetCountFood()
    {
      return Json(new FoodDao().GetAllFood().Count, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult InsertFood(string foodName,int idFoodType, string description,int priceImport,int priceOutput)
    {
      return Json(new FoodDao().InsertFood(foodName,idFoodType,description, priceImport, priceOutput), JsonRequestBehavior.AllowGet);
    }
    [HttpPost]
    public JsonResult UpdateFood(int idFood,string foodName, int idFoodType, string description, int priceImport, int priceOutput)
    {
      return Json(new FoodDao().UpdateFood(idFood,foodName, idFoodType, description, priceImport, priceOutput), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public JsonResult RemoveFood(int idFood)
    {
      return Json(new FoodDao().RemoveFood(idFood), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public JsonResult GetFoodByType(int idFoodType)
    {
      return Json(new FoodDao().GetFoodByType(idFoodType), JsonRequestBehavior.AllowGet);
    }

    public JsonResult Search(string term)
    {
      var data = new FoodDao().Search(term);
      return Json(data, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult SearchFood(int foodTypeID, string keyword,int page)
    {
      return Json(new FoodDao().SearchFood(foodTypeID, keyword, page), JsonRequestBehavior.AllowGet);
    }

  }
}

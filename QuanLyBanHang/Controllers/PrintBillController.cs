using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeUtility;

namespace QuanLyBanHang.Controllers
{
  public class PrintBillController : Controller
  {
    // GET: PrintBill
    public ActionResult Index()
    {
      return View();
    }
    [HttpGet]
    public ActionResult Index(int ID)
    {
      var model = new BillDao().GetDetailBill(ID);
      return View(model);
    }


    public ActionResult PrintCardEmploy(string username)
    {
      var model = new BillDao().PrintCardEmploy(username);
      return View(model);
    }
  }
}

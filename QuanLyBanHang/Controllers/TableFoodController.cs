using Models.DAO;
using QuanLyBanHang.Helper;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class TableFoodController : Controller
  {
    // GET: TableFood
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult getAllTable()
    {
      return Json(new TableFoodDao().getAllTableFood(), JsonRequestBehavior.AllowGet);
    }

    public JsonResult getAllTableEmpty()
    {
      return Json(new TableFoodDao().getAllTableFoodEmpty(), JsonRequestBehavior.AllowGet);
    }

    public JsonResult getAllTableNotEmpty()
    {
      return Json(new TableFoodDao().getAllTableFoodNotEmpty(), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public JsonResult getTableByID(int idTable)
    {
      return Json(new TableFoodDao().getTableByID(idTable), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult Order(int tableID, int idFood,string description, int quantity)
    {
      return Json(new TableFoodDao().Order(tableID, idFood, description, quantity,SessionUtility.Account), JsonRequestBehavior.AllowGet);
    }

    [HttpGet]
    public JsonResult GetListOrderByTable(int idTable)
    {
      return Json(new TableFoodDao().GetListOrderByTable(idTable), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult RemoveFoodDetail(int idTable, int idFood)
    {
      return Json(new TableFoodDao().RemoveFoodDetail(idTable, idFood));
    }

    [HttpPost]
    public JsonResult ChangeTable(int fromIDTable, int toIDTable)
    {
      return Json(new TableFoodDao().ChangeTable(fromIDTable, toIDTable,SessionUtility.Account));
    }

    [HttpPost]
    public JsonResult GeneralTable(int fromIDTable, int toIDTable)
    {
      return Json(new TableFoodDao().GeneralTable(fromIDTable, toIDTable, SessionUtility.Account));
    }

    [HttpPost]
    public JsonResult InserTable(string tableName, int position)
    {
      return Json(new TableFoodDao().InsertTable(tableName, position), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult UpdateQuantityDetail(int idTable, int idFood, int quantity)
    {
      return Json(new TableFoodDao().UpdateQuantityEdit(idTable, idFood, quantity));
    }

    [HttpPost]
    public JsonResult SearchTable(string statusTable, string keyword, int page)
    {
      return Json(new TableFoodDao().SearchTable(statusTable, keyword, page));
    }

    [HttpPost]
    public JsonResult UpdateTable(int idTable, string nameTable, int position)
    {
      return Json(new TableFoodDao().UpdateTable(idTable, nameTable, position));
    }

    [HttpPost]
    public JsonResult RemoveTable(int idTable)
    {
      return Json(new TableFoodDao().RemoveTable(idTable));
    }

    public JsonResult GetCountTable()
    {
      int countTable = new TableFoodDao().getAllTableFood().Count;
      return Json(new { countTable }, JsonRequestBehavior.AllowGet);
    }
  }
}

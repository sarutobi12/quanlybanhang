using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class ThongKeController : Controller
  {
    // GET: ThongKe
    public ActionResult Index()
    {
      return View();
    }

    [HttpGet]
    public JsonResult GetCountFood()
    {
      return Json(new ThongKeDao().GetCountFood(), JsonRequestBehavior.AllowGet);
    }
    [HttpGet]
    public JsonResult GetQuantityFood()
    {
      return Json(new ThongKeDao().GetQuantityFood(), JsonRequestBehavior.AllowGet);
    }
    [HttpGet]
    public JsonResult GetQuantityNearFood()
    {
      return Json(new ThongKeDao().GetQuantityNearFood(), JsonRequestBehavior.AllowGet);
    }
    [HttpGet]
    public JsonResult GetCountFoodOutOfStock()
    {
      return Json(new ThongKeDao().GetCountFoodOutOfStock(), JsonRequestBehavior.AllowGet);
    }
  }
}

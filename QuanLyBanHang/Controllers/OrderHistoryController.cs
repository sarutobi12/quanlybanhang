using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class OrderHistoryController : Controller
  {
    // GET: OrderHistory
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult GetDataHistory()
    {
      return Json(new OrderHistoryDao().GetDataHistory(), JsonRequestBehavior.AllowGet);
    }

    public JsonResult SearchHistory(int page=1)
    {
      return Json(new OrderHistoryDao().SearchHistory(page), JsonRequestBehavior.AllowGet);
    }
  }
}

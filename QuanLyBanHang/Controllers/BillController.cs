using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class BillController : Controller
  {
    // GET: Bill
    public ActionResult Index()
    {
      return View();
    }

    public JsonResult SearchBill(int tableID, string keyword, string fromTime, string toTime, int page)
    {
      return Json(new BillDao().SearchBill(tableID, keyword, fromTime, toTime, page), JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetBillDetailByBillID(int billID)
    {
      return Json(new BillDao().GetBillDetailByBillID(billID),JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetCountBill()
    {
      return Json(new BillDao().GetCountBill(), JsonRequestBehavior.AllowGet);
    }
  }
}

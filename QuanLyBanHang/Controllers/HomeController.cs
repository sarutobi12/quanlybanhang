using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuanLyBanHang.Controllers
{
  public class HomeController : BaseController
  {
    // GET: Home
    public ActionResult Index()
    {
      return View();
    }


    public ActionResult Header()
    {
      return PartialView();
    }

    public ActionResult MenuLeft()
    {
      return PartialView();
    }

    public ActionResult Logout()
    {
      Session.Abandon();
      return RedirectToAction("Index", "Login");
    }
  }
}

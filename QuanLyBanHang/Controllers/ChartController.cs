using Models.DAO;
using Models.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLyBanHang.Controllers
{
  public class ChartController : Controller
  {
    // GET: Chart
    public ActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public JsonResult GetDataChart(string fromDate, string endDate)
    {
      var data = new Chart().GetDataChart(fromDate, endDate);
      return Json(data, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult GetDataChartProduct(string fromDate, string endDate)
    {
      var data = JsonConvert.SerializeObject(new Chart().GetDataChartProduct(fromDate, endDate));
      return Json(data, JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public ActionResult ExportExcel(string fromDate, string endDate)
    {
      object dataObj = new Chart().GetDataChart(fromDate, endDate);


      var gv = new GridView();

      Type myType = dataObj.GetType();
      IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
      List<ChartModel> data = new List<ChartModel>();
      foreach (PropertyInfo prop in props)
      {
        //object propValue = prop.GetValue(dataObj, null);
        if (prop.Name == "listData")
        {
          data = prop.GetValue(dataObj, null) as List<ChartModel>;
          break;
        }
      }

      gv.DataSource = data.Select(x => new { Date = x.drilldown, y= x.y });
      gv.DataBind();
      Response.Clear();
      Response.Buffer = true;

      StringWriter sw = new StringWriter();
      HtmlTextWriter hw = new HtmlTextWriter(sw);

      //Add màu nền cho header của file excel
      gv.HeaderRow.ForeColor = System.Drawing.Color.Blue;

      //Màu chữ cho header của file excel

      gv.HeaderRow.Cells[0].Text = "Date";
      gv.HeaderRow.Cells[1].Text = "Amount";
      gv.RenderControl(hw);

      Response.Output.Write(sw.ToString());
      Response.Flush();
      Response.End();
      return View();
    }
  }
}

using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CodeUtility;
using QuanLyBanHang.Helper;

namespace QuanLyBanHang.Controllers
{
  public class AccountController : Controller
  {
    // GET: Account
    public ActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public JsonResult InsertAccount(string username, string password, int accountMainID, string fullName, string address, string mobi, string email, bool status)
    {
      string hassPass = password.SHA256Hash();
      string hassQRCode = (username + "/" + password).SHA256Hash();
      return Json(new AccountDao().InsertAccount(username, hassPass, accountMainID, fullName, address, mobi, email, status, hassQRCode,SessionUtility.Account), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult UpdateAccount(string username, string password, int accountMainID, string fullName, string address, string mobi, string email, bool status)
    {
      string hassPass = string.Empty;
      string hassQRCode = string.Empty;
      if (!password.Trim().IsNullOrEmpty())
      {
        hassQRCode = (username + "/" + password).SHA256Hash();
        hassPass = password.SHA256Hash();

      }


      return Json(new AccountDao().UpdateAccount(username, hassPass, accountMainID, fullName, address, mobi, email, status, hassQRCode, SessionUtility.Account), JsonRequestBehavior.AllowGet);
    }

    [HttpPost]
    public JsonResult RemoveAccount(string username)
    {
      return Json(new AccountDao().RemoveAccount(username), JsonRequestBehavior.AllowGet);
    }
    [HttpPost]
    public JsonResult SearchAccount(int accountMainID, string keyword, int page)
    {
      return Json(new AccountDao().SearchAccount(accountMainID, keyword, page), JsonRequestBehavior.AllowGet);
    }

    public JsonResult GetCountAcount()
    {
      return Json(new AccountDao().GetCountAccount(), JsonRequestBehavior.AllowGet);
    }
  }
}

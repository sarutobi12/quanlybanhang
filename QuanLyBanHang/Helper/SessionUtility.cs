using Models.EF;
using Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLyBanHang.Helper
{
  public static class SessionUtility
  {
    public static AccountModel Account
    {
      get
      {
        return HttpContext.Current.Session["Account"] as AccountModel;
      }
      set
      {
        HttpContext.Current.Session["Account"] = value;
      }
    }

    public static Cart Cart
    {
      get
      {
        if (HttpContext.Current.Session["Cart"] == null)
          HttpContext.Current.Session["Cart"] = new Cart();
        return HttpContext.Current.Session["Cart"] as Cart;
      }
      set
      {
        HttpContext.Current.Session["Cart"] = value;
      }
    }
  }
}

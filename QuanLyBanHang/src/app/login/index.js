import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './Login.vue'

new Vue({
  el: '#login',
  render: h => h(Login)
})

//Define router to component
import TableListComponent from './table/TableListComponent.vue'
import HomeComponent from './home/HomeComponent.vue'
import HomeProductComponent from './product/HomeProductComponent.vue'
import HomeImportComponent from './import/HomeImportComponent.vue'
import HomeTableManagerComponent from './table-manager/HomeTableManagerComponent.vue'
import HomeAccountComponent from './account/HomeAccountComponent.vue'
import HomeReportRevenueComponent from './report-revenue/HomeReportRevenueComponent.vue'
import HomeImportHistoryComponent from './import-history/HomeImportHistoryComponent.vue'
import HomeBillManagerComponent from './bill-manager/HomeBillManagerComponent.vue'
import HomeNotiHistoryComponent from './noti-history/HomeNotiHistoryComponent.vue'
import PageNotFound from './error/PageNotFound.vue';

export const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { title: 'Trang chủ' },
    component: HomeComponent
  },
  {
    path: '/table-list',
    name: 'table-list',
    meta: { title: 'Danh sách bàn' },
    component: TableListComponent
  },
  {
    path: '/product',
    name: 'product',
    meta: { title: 'Quản lý sản phẩm' },
    component: HomeProductComponent
  },
  {
    path: '/import',
    name: 'import',
    meta: { title: 'Nhập hàng' },
    component: HomeImportComponent
  },
  {
    path: '/table-manager',
    name: 'table-manager',
    meta: { title: 'Quản lý bàn' },
    component: HomeTableManagerComponent
  },
  {
    path: '/account',
    name: 'account',
    meta: { title: 'Danh mục tài khoản' },
    component: HomeAccountComponent
  },
  {
    path: '/report-revenue',
    name: 'report-revenue',
    meta: { title: 'Thống kê doanh thu' },
    component: HomeReportRevenueComponent
  },
  {
    path: '/import-history',
    name: 'import-history',
    meta: { title: 'Lịch sử nhập hàng' },
    component: HomeImportHistoryComponent
  },
  {
    path: '/bill-manager',
    name: 'bill-manager',
    meta: { title: 'Thống kê hóa đơn bán hàng' },
    component: HomeBillManagerComponent
  },
  {
    path: '/noti-history',
    name: 'noti-history',
    meta: { title: 'Thông báo từ hệ thống' },
    component: HomeNotiHistoryComponent
  },
  {
    path: "*",
    component: PageNotFound,
    meta: { title: '404 Page! Không tìm thấy trang' },
  }
];

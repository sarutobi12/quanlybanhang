
//Loading bar
var ShowLoadingBar = function () {
  NProgress.inc();
};

var HideLoadingBar = function () {
  setTimeout(function () {
    NProgress.done();
  }, 1500);
}

var FadeLoadingBar = function () {
  ShowLoadingBar();
  HideLoadingBar();
}

//LoadingDelay

var HideLoading = function (timeDelay = 500, timeOut = 500) {
  $("#main-loading-delay").delay(timeDelay).fadeOut(timeOut);
};
var ShowLoading = function (timeOut = 500) {
  $("#main-loading-delay").fadeIn(timeOut);
}

var FadeLoading = function (timeOut = 500) {
  $("#main-loading-delay").fadeIn().fadeOut(timeOut);
}

//Hàm tiện ích
//Alert
var Alert = function (title, icon, type, content) {
  $.alert({
    title: title,//Error,success..
    icon: icon,//Font Awesome
    type: type,//Green..
    content: content,
    theme: "modern",
    buttons: {
      OK: {
        text: "Đã hiểu",
        btnClass: "btn-red",
      }
    }
  });
};
var AlertBasic = function (icon, title, content, type, theme = "modern") {
  $.confirm({
    icon: icon,
    title: title,
    theme: theme,
    closeIcon: true,
    content: content,
    animation: "scale",
    type: type,
    buttons: {
      Đóng: {
        text: "Đã hiểu",
        btnClass: "btn-dark"
      }
    }
  });
};
//Clock
var GetClock = setInterval(Time, 0);

function Time() {
  var d = new Date();
  var t = formatAMPM(d);

  $("#eval-clock").html(d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + "  |  " + t)

  $("#eval-date").html(d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear())
}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

//Hàm chuyển tiền
function numberToText(total) {
  try {
    var rs = "";
    total = Math.round(total, 0);
    var ch = [
      "không",
      "một",
      "hai",
      "ba",
      "bốn",
      "năm",
      "sáu",
      "bảy",
      "tám",
      "chín"
    ];
    var rch = [
      "lẻ",
      "mốt",
      "",
      "",
      "",
      "lăm"
    ];
    var u = [
      "",
      "mươi",
      "trăm",
      "ngàn",
      "",
      "",
      "triệu",
      "",
      "",
      "tỷ",
      "",
      "",
      "ngàn",
      "",
      "",
      "triệu"
    ];
    var nstr = total.toString();

    var n = new Array(nstr.length);
    var len = n.length;

    for (var i = 0; i < len; i++) {
      n[len - 1 - i] = parseInt(nstr.substr(i, 1));
    }

    for (var i = len - 1; i >= 0; i--) {
      if (i % 3 == 2) // số 0 ở hàng trăm
      {
        if (n[i] == 0 && n[i - 1] == 0 && n[i - 2] == 0) continue; //nếu cả 3 số là 0 thì bỏ qua không đọc
      }
      else if (i % 3 == 1) // số ở hàng chục
      {
        if (n[i] == 0) {
          if (n[i - 1] == 0) { continue; }// nếu hàng chục và hàng đơn vị đều là 0 thì bỏ qua.
          else {
            rs += " " + rch[n[i]]; continue;// hàng chục là 0 thì bỏ qua, đọc số hàng đơn vị
          }
        }
        if (n[i] == 1)//nếu số hàng chục là 1 thì đọc là mười
        {
          rs += " mười"; continue;
        }
      }
      else if (i != len - 1)// số ở hàng đơn vị (không phải là số đầu tiên)
      {
        if (n[i] == 0)// số hàng đơn vị là 0 thì chỉ đọc đơn vị
        {
          if (i + 2 <= len - 1 && n[i + 2] == 0 && n[i + 1] == 0) continue;
          rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);
          continue;
        }
        if (n[i] == 1)// nếu là 1 thì tùy vào số hàng chục mà đọc: 0,1: một / còn lại: mốt
        {
          rs += " " + ((n[i + 1] == 1 || n[i + 1] == 0) ? ch[n[i]] : rch[n[i]]);
          rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);
          continue;
        }
        if (n[i] == 5) // cách đọc số 5
        {
          if (n[i + 1] !== 0) //nếu số hàng chục khác 0 thì đọc số 5 là lăm
          {
            rs += " " + rch[n[i]];// đọc số
            rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);// đọc đơn vị
            continue;
          }
        }
      }

      rs += (rs == "" ? " " : ", ") + ch[n[i]];// đọc số
      rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);// đọc đơn vị

    }

    if (rs[rs.length - 1] != ' ') {
      rs += " đồng.";
    }
    else {
      rs += "đồng.";
    }

    if (rs.length > 2) {
      var rs1 = rs.substr(0, 2);
      rs = rs.substr(2);
      rs = rs1 + rs;
    }
    rs = rs.trim(" ").replace(/lẻ,/g, "lẻ").replace(/mươi,/g, "mươi").replace(/trăm,/g, "trăm").replace(/mười,/g, "mười");
    rs = rs.charAt(0).toUpperCase() + rs.slice(1);
    rs = rs.trim().replace(/,/g, "");
    return rs;
  }
  catch (err) {
    return err;
  }
}


var ConverPrice = function (price, type) {
  if (price === null || price === 0) {
    return "0" + type;
  }
  price = price.toString();
  var n = price
    .split("")
    .reverse()
    .join("");
  var n2 = n.replace(/\d\d\d(?!$)/g, "$&.");
  return (
    n2
      .split("")
      .reverse()
      .join("") + type
  );
}
//Loading
var LoadingShow = function (timeOut = 0) {
  $("#main-loading").fadeIn(timeOut);
}
var LoadingHide = function (timeOut = 0) {
  $("#main-loading").fadeOut(timeOut);
}

//Created Init button
var ControlButton = {
  Init: function () {
    this.Register();
  },
  Register: function () {
    //Khi thêm món
    $(".btn-open-order").on('click', function () {
      // $(this).html("<i class='fa fa-lg fa-angle-up'></i>Ẩn thêm món");
      //HideAllBoxControlPanel();
      if (!$(".box-order").is(":visible")) {
        $(".box-change-table").hide();
        $(".box-payment").hide();
        $(".box-general-table").hide();
      }
      $(".box-order").slideToggle();
    });
    //Khi chuyển bàn
    $(".btn-change-table").on('click', function () {
      if (!$(".box-change-table").is(":visible")) {
        $(".box-order").hide();
        $(".box-payment").hide();
        $(".box-general-table").hide();
      }
      $(".box-change-table").slideToggle();
    });

    $(".btn-payment").on('click', function () {
      if (!$(".box-payment").is(":visible")) {
        $(".box-order").hide();
        $(".box-change-table").hide();
        $(".box-general-table").hide();
      }
      $(".box-payment").slideToggle();
    });

    $(".btn-general-table").on('click', function () {
      if (!$(".box-general-table").is(":visible")) {
        $(".box-order").hide();
        $(".box-change-table").hide();
        $(".box-payment").hide();
      }
      $(".box-general-table").slideToggle();
    });
  }
}


//Mask
var Mask = {
  Init: function () {
    this.Register();
  },
  Register: function () {
    $(".price-mask").unmask();
    $('.price-mask').mask('000.000.000 đ', { reverse: true });
  }
}

//Create ChartRevenue
var ChartRevenue = {
  Init: function (dataDate, dataTableByDate) {
    this.Register(dataDate, dataTableByDate);
  },
  Register: function (dataDate, dataTableByDate) {
    //Khởi tạo chart
    Highcharts.chart('chart-container', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Biểu đồ thống kê doanh thu'
      },
      subtitle: {
        text: 'Nhấn vào từng cột để xem chi tiết doanh thu của bàn'
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Mức doanh thu'
        }

      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y:,.0f} đ'
          }
        }
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}đ</b> của tổng doanh thu ngày<br/>'
      },

      "series": [
        {
          "name": "Doanh thu",
          "colorByPoint": true,
          "data": dataDate
        }
      ],
      "drilldown": {
        "series": dataTableByDate
      }
    });
  }
}
//Create ChartRevenue
var ChartProduct = {
  Init: function (dataChart) {
    this.Register(dataChart);
  },
  Register: function (dataChart) {
    //Khởi tạo chart

    var dataChartConvert = JSON.parse(dataChart);
    console.log("==========");
    console.log(dataChartConvert);

    let categories = [];
    let data = [];
    dataChartConvert.listFoodChart.map(item => {
      categories.push(item.Value.FoodName);
      data.push(item.Value.Quantity);
    });

    Highcharts.chart('product-container', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Thống kê 10 sản phẩm bán chạy'
      },
      xAxis: {
        categories: categories,
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Đơn vị tính (sản phẩm)',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: ' sản phẩm'
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: -80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Số lượng bán ra',
        data: data
      }]
    });
  }
}

var QrCode = function (idModal) {
  debugger;
  return new Promise(function (resolve, reject) {
    $(idModal).modal("show");
    var args = {
      autoBrightnessValue: 100,
      resultFunction: function (res) {
        //Nếu scan có data
        if (res.code) {
          resolve(res.code);
        }
      }
    };
    var decoder = new WebCodeCamJS("#webcodecam-canvas").init(args);
    if (!decoder.isInitialized()) {
    } else {
      decoder.play();
    }
    //Nếu tắt modal thì đồng thời tắt camera
    $(idModal).on('hide.bs.modal', function () {
      decoder.stop();
    });
  });

}

var DateTimeFormat = {
  GetDate: function (dateTime) {
    var nowDate = new Date(parseInt(dateTime.substr(6)));
    return nowDate.format("dd/mm/yyyy");
  },
  GetTime: function (dateTime) {
    var nowDate = new Date(parseInt(dateTime.substr(6)));
    return nowDate.format("HH:MM");
  },
  GetFullTime: function (dateTime) {
    var nowDate = new Date(parseInt(dateTime.substr(6)));
    return nowDate.format("HH:MM:ss");
  },
  GetDateTime: function (dateTime) {
    var nowDate = new Date(parseInt(dateTime.substr(6)));
    return nowDate.format("dd/mm/yyyy -- HH:MM:ss");
  }
}

var Tooltip = {
  Init: function (className, dataInput) {
    $(className).tooltipster({
      content: dataInput,
      contentCloning: true,
      contentAsHTML: true,
      interactive: true,
      functionInit: function (instance, helper) {
        var data = instance.content();
        if (data != 0) {
          let result = "";
          data.map(res => {
            let temp = `
                     <li>
                        <span class="handle ui-sortable-handle">
                          <i class="fa fa-ellipsis-v"></i>
                          <i class="fa fa-ellipsis-v"></i>
                        </span>
                        <span class="text">{name}</span>
                        <small class="badge bg-yellow pull-right"><i class="fa fa-refresh"></i> &nbspCòn {quantity} SP</small>

                      </li>
                `;
            result += temp.replace("{name}", res.FoodName).replace("{quantity}", res.Quantity);
          });

          let resultAll = `<ul class="todo-list ui-sortable">` + result + `</ul>`;

          instance.content(resultAll);
        }
        else {
          instance.content("Không có sản phẩm nào");
        }
      },
    });
  }
}

var JSONDateWithTime = function (dateStr) {
  jsonDate = dateStr;
  var d = new Date(parseInt(jsonDate.substr(6)));
  var m, day;
  m = d.getMonth() + 1;
  if (m < 10)
    month = '0' + m
  if (d.getDate() < 10)
    day = '0' + d.getDate()
  else
    day = d.getDate();

  var year = d.getFullYear();
  var formattedDate = day + "/" + month + "/" + year;
  var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
  var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
  var seconds = d.getSeconds();
  var formattedTime = hours + ":" + minutes;
  formattedDate = formattedTime + " - " + formattedDate;

  //Ngày giờ hiện tại
  var dateObj = new Date();
  var dayNow = dateObj.getDate();
  var monthNow = dateObj.getMonth() + 1;
  var yearNow = dateObj.getFullYear();
  var hoursNow = dateObj.getHours();
  var minutesNow = dateObj.getMinutes();
  var secondsNow = dateObj.getSeconds();

  dateObj = formattedDate;

  if (hoursNow - hours == 0 && minutesNow - minutes == 0 && yearNow - year == 0 && monthNow - month == 0 && dayNow - day == 0) {
    dateObj = secondsNow - seconds + " giây trước";
  }

  if (hoursNow - hours == 0 && minutesNow - minutes > 0 && yearNow - year == 0 && monthNow - month == 0 && dayNow - day == 0) {
    dateObj = minutesNow - minutes + " phút trước";
  }

  if (hoursNow - hours > 0 && yearNow - year == 0 && monthNow - month == 0 && dayNow - day == 0) {
    dateObj = hoursNow - hours + " giờ trước";
  }

  if (yearNow - year == 0 && monthNow - month == 0 && dayNow - day > 0 && dayNow - day <= 7) {
    dateObj = dayNow - day + " ngày trước";
  }

  if (yearNow - year == 0 && monthNow != month) {
    var day = dayNow - day;
    var month = monthNow - month;
    day = month * 30 + day;
    if (0 < day && day <= 7) {
      dateObj = day + " Ngày trước";
    }

  }



  return dateObj;
}



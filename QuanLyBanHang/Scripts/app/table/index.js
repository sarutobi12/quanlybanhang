import Vue from 'vue'
import TableListComponent from './TableListComponent.vue'
import TableDetailComponent from './TableDetailComponent.vue'


var tableList = new Vue({
  el: '#list-table-food',
  components: {
    TableListComponent,
  },
})

var tableDetail = new Vue({
  el:'#modal-table-detail',
  components:{
    TableDetailComponent
  },
})
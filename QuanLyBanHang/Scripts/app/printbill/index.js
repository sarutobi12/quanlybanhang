import Vue from 'vue'
import PrintBillComponent from './PrintBillComponent.vue'
var PrintBill = new Vue({
    el: '#bill-content',
    components: {
        PrintBillComponent,
    },
  })
﻿
var Login = {
    Init: function () {
        LoadingHide();
        this.Register();
    },
    Register: function () {
        let seft = this;
        $("#login").click(function () {
            let username = $("#username").val().trim();
            let password = $("#password").val().trim();

            if (!username) {
                Alert("Error", "fa fa-warning", "red", "Username không được để trống");
                return;
            }
            if (!password) {
                Alert("Error", "fa fa-warning", "red", "Password không được để trống");
                return;
            }
            LoadingShow();
            $.get("/Login/Login", { username: username, password: password }, function (res) {

                if (res.isLogin == -1) {
                    LoadingHide();
                    Alert("Error", "fa fa-warning", "red", "Tài khoản không tồn tại! Vui lòng kiểm tra lại");
                }
                else if (res.isLogin == 0) {
                    LoadingHide();
                    Alert("Error", "fa fa-warning", "orange", "Tài khoản đang tạm khóa! Vui lòng liên hệ admin để kích hoạt lại");
                }
                else if (res.isLogin == 1) {
                    LoadingHide();
                    Alert("Error", "fa fa-warning", "red", "Sai mật khẩu! Vui lòng thử lại");
                }
                else {
                    LoadingHide();
                    window.location.href = "/";
                }
            }

            })
    })

}
};

Login.Init();
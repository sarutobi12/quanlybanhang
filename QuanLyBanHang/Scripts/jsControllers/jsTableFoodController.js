﻿var TableFoodArea = {
    Init: function () {
        this.Register();
    },
    Register: function () {
        let promiseAllTable = this.Method.GetDataAjax("/TableFood/getAllTable/", "GET");
        promiseAllTable.then(function (res) {
            let tempTable = "";
            res.data.map(function (item) {
                //Định nghĩa lại teamplate
                var tableReplace = TableFoodArea.Teamplate.teamTable.replace("{table-name}", item.TableName);
                tableReplace = tableReplace.replace("{class}", (item.Status == true ? "code-danger" : "code-success"));
                tableReplace = tableReplace.replace("{text-status}", (item.Status == true ? "Đã có khách" : "Bàn trống"));
                tableReplace = tableReplace.replace("{total}", TableFoodArea.Method.ConverPrice(item.Total, "₫"));
                tableReplace = tableReplace.replace("{id}", item.TableID);
                tempTable += tableReplace;
            });
            //Chèn bàn vào home
            $("#list-table-food").append(tempTable);
            //Click vào đối tượng bàn
            $("#list-table-food").on('click', '.thisTable', function () {
                let idTable = $(this).data("id");

                //Load dữ liệu vào modal
                TableFoodArea.Method.LoadDataModalTableDetail(idTable, "#modal-detail-table");
            });
        }, function (error) {
            console.log(error.message);
        });

    },
    Data: {
        idFoodType: 0,
        idFood:0
    },
    Teamplate: {
        teamTable: `
                    <div class="col-xs-3">
                        <!-- small box -->
                          <a href="#" data-id={id} class="small-box bg-table thisTable">
                              <div class="inner item-table">
                                  <p>{table-name}</p>
                                  <p>Trạng thái: <code class="{class} text-nowrap">{text-status}</code></p>
                                   <p>Tổng tiền: <span  class="total">{total}</span></p>
                              </div>
                              <div class="icon">
                                  <i class="ion ion-ios-paper"></i>
                              </div>
                              <span class="small-box-footer">Đặt bàn <i class="fa fa-plus-square"></i></span>
                           </a>
                    </div>
                    `
    },
    Method: {
        GetDataAjax: function (url, method, data = "") {
            let promise = new Promise(function (resolve, reject) {
                if (method == "GET") {
                    $.get(url + data, function (res, status) {
                        if (status == "success") {
                            resolve(res);
                        }
                        else {
                            reject(Error('Error fetching data.'));
                        }
                    });
                }
                else {
                    //Ngược lại là phương thức post
                    $.post(url, data, function (res, status) {
                        if (status == "success") {
                            resolve(res);
                        }
                        else {
                            reject(Error('Error fetching data.'));
                        }
                    });
                }

            });

            return promise;
        },
        ConverPrice: function (price, type) {
            if (price === null || price === 0) {
                return "0" + type;
            }
            price = price.toString();
            var n = price.split('').reverse().join("");
            var n2 = n.replace(/\d\d\d(?!$)/g, "$&.");
            return n2.split('').reverse().join('') + type;
        },
        LoadDataModalTableDetail: function (idTabless, idModal) {
            //Hiển thị modal lên
            TableFoodArea.Method.ShowModal(idModal);
            //Lấy dữ liệu chi tiết  table đổ vào modal
            var promiseTable = this.GetDataAjax("/TableFood/getTableByID/", "GET", idTabless);
            promiseTable.then(res => {
                $(idModal).find(".modal-title").html("Thông tin chi tiết <code class='code-danger'>" + res.TableName.toLowerCase() + "</code>");
                if (res.Status == true) {

                    //Bàn đang có người
                    $(idModal).find("#cb_StatusTrue").attr('checked', true);
                    $(idModal).find("#cb_StatusFalse").removeAttr('checked');
                }
                else {
                    $(idModal).find("#cb_StatusTrue").removeAttr('checked');
                    $(idModal).find("#cb_StatusFalse").attr('checked', true);
                }

                //Load all Food Type đổ vào select
                TableFoodArea.Method.LoadAllFoodType();
                //Bắt sự kiện thay đổi ở select type food
                TableFoodArea.Method.ChangeSelectAllFoodType();
                //mặc định cho select món
                $("#sl-food-by-type").html(" <option value='0'>Chọn món</option>");
                //Sự kiện thay đổi khi chọn food
                TableFoodArea.Method.ChangeSelectFood();
                //Load tìm kiếm
                TableFoodArea.Method.SearchFood();

                //Add Order Select
                TableFoodArea.Method.AddOrder.AddOrderSelect();
            });

        },
        ShowModal: function (idModal) {
            $(idModal).modal('show');
        },
        LoadAllFoodType: function () {
            var promiseAllFoodType = this.GetDataAjax("/FoodType/GetAllFoodType");
            promiseAllFoodType.then(res => {
                var tempOption = " <option value='0'>Chọn loại</option>";
                res.map(function (item) {
                    var temp = " <option value='" + item.FoodTypeID + "'>" + item.Name + "</option>";
                    tempOption += temp;
                });
                $("#sl-all-food-type").html(tempOption);
            });
        },
        LoadFoodByType: function (idFoodType) {
            var promiseAllFood = this.GetDataAjax("/Food/GetFoodByType", "POST", { idFoodType: idFoodType });
            promiseAllFood.then(res => {
                var tempOption = " <option value='0'>Chọn món</option>";
                res.map(function (item) {
                    console.log(item);
                    var temp = " <option value='" + item.FoodID + "'>" + item.FoodName + "</option>";
                    tempOption += temp;
                });
                $("#sl-food-by-type").html(tempOption);
            });
        },
        ChangeSelectAllFoodType: function () {
            $("#sl-all-food-type").change(function () {
                let idFoodType = TableFoodArea.Data.idFoodType;
                idFoodType= $(this).val();
                if (idFoodType != 0) {
                    $("#sl-food-by-type").removeAttr("disabled");
                    TableFoodArea.Method.LoadFoodByType(idFoodType);
                }
                else {
                    $("#sl-food-by-type").attr("disabled", "disabled");
                    //đổ dữ liệu mặc định
                    $("#input-quantity-order").val();
                    $("#sl-food-by-type").html(" <option value='0'>Chọn món</option>");
                    return;
                }
            });
        },
        ChangeSelectFood: function () {
            $("#sl-food-by-type").change(function () {
                TableFoodArea.Data.idFood = $(this).val();
                if (TableFoodArea.Data.idFood != 0) {
                    $("#input-quantity-order").removeAttr("disabled");
                }
                else {
                    $("#input-quantity-order").attr("disabled", "disabled");
                }
            });
        },
        SearchFood: function () {
            $('#search-food').select2({
                ajax: {
                    url: "/Food/Search/",
                    type: "GET",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            term: params.term
                        };
                    },
                    processResults: function (data, params) {
                        console.log(data);
                        return {
                            results: $.map(data, function (obj) {
                                return {
                                    id: obj.FoodID,
                                    text: obj.FoodName
                                };
                            })
                        };
                    },
                    cache: true
                },
                placeholder: 'Gõ để tìm kiếm',
                minimumInputLength: 1,
            });
        },
        AddOrder: {
            AddOrderSelect: function () {

                $("#add-Order-Select").on('click', function () {
                    debugger;
                    alert("ok");
                    let idFood = TableFoodArea.Data.idFood;
                    var stt = 0;
                    alert(stt++);
                    if (idFood == 0) {

                        Alert("Error", "fa fa-warning", "red", "Vui lòng chọn món");
                        return;
                    }
                    let quantity = $("#input-quantity-order").val();
                    if (quantity<=0) {
                        Alert("Error", "fa fa-warning", "red", "Vui lòng chọn số lượng lớn hơn 1");
                        return;
                    }
                    //Gửi dữ liệu lên server
                   let PromiseAddOrder = TableFoodArea.Method.GetDataAjax("/Table/Order", { idFood: idFood, quantity: quantity });
                    PromiseAddOrder.then(res => {

                    });
                });
            }
        }
    }
};

TableFoodArea.Init();
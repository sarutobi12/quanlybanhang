
Highcharts.chart('container', {
  chart: {
    type: 'bar'
  },
  title: {
    text: 'Thống kê TOP 10 sản phẩm bán chạy'
  },
  subtitle: {
    text: 'Danh sách được thống kê trong 7 ngày hiện tại'
  },
  xAxis: {
    categories: ['Cà phê sữa đá', 'Sting', 'Nước cam', 'Ca cao đá nóng', 'Bò húc'],
    title: {
      text: null
    }
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Population (millions)',
      align: 'high'
    },
    labels: {
      overflow: 'justify'
    }
  },
  tooltip: {
    valueSuffix: ' millions'
  },
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true
      }
    }
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    x: -40,
    y: 80,
    floating: true,
    borderWidth: 1,
    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    shadow: true
  },
  credits: {
    enabled: false
  },
  series: [{
    name: 'Số lượng bán ra',
    data: [635, 203,107, 31, 2]
  }]
});
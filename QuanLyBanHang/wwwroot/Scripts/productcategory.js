function auto_load(){
        $.ajax({
          url: "ucControl/categoryparentlist.php",
          cache: false,
          success: function(data){
             $("#formlist").html(data);
          } 
        });
}


$(document).ready(function(){
	$('#btnmodelparent').on('click', function(){
	var data =$('#form-parent').serialize();
	$.ajax({
	type : 'POST', 
	url  : 'ucControl/productcategoryparent.php',
	data : data,
	success :  function(data)
		   {						
				var status = data.toString().split(0,1);
				var title = data.toString().slice(2);
				
				if (status == 0) {
					alert("" + title);
				}
				else
				{
					alert("Cập nhật thành công");
				}
		   }
		});
	});
	$('#btnmodelchildren').on('click', function(){
	var data =$('#form-children').serialize();
	$.ajax({
	type : 'POST', 
	url  : 'ucControl/productcategorychildren.php',
	data : data,
	success :  function(data)
		   {						
				var status = data.toString().split(0,1);
				var title = data.toString().slice(2);
				
				if (status == 0) {
					alert("" + title);
				}
				else
				{
					alert("Cập nhật thành công");
				}
		   }
		});
	});
	//setInterval(function(){
	//$('.box-parent').load("../ucControl/categoryparentlist.php").fadeIn("slow");
	//},1000);
	
	$('.btnedit').on('click', function(){
	var data_id = $(this).data('id');
	$.ajax({
		url: 'ucControl/productcatedetail.php',
		type: 'POST',
		data: {id: data_id},
		success: function(data){
			//$('#formlist').load(document.URL +  '#formlist');
			var obj = JSON.parse(data);
			$("#txtID").val(obj.ID);
			$("#txtName").val(obj.Name);
			$("#txtPosition").val(obj.Position);
			$("#txtDescription").val(obj.Description);
			$("#txtKeyDescription").val(obj.MetaDescription);
			$("#txtKeyword").val(obj.MetaKeyword);
			if(obj.Status == 1)
			{
				$("#checkbox1_8").attr("checked","false");
				$("#checkbox1_9").attr("checked","true");
			}
			else
			{
				$("#checkbox1_8").attr("checked","true");
				$("#checkbox1_9").attr("checked","false");
			}
			
			$("#Input_CreatedBy").val(obj.CreatedBy);
			$("#Input_CreatedDate").val(obj.CreatedDate == null ? "" : obj.CreatedDate);
			$("#Input_UpdatedBy").val(obj.UpdatedBy);
			$("#Input_UpdatedDate").val(obj.UpdatedDate == null ? "" : obj.UpdatedDate);
			},
			error: function(jqXHR, textStatus, errorThrown){
				toastr["error"]("cập nhập trạng thái không thành công");
				
			}
		});
	});
	
	$('.btneditchildren').on('click', function(){
	var data_id = $(this).data('id');
	$.ajax({
		url: 'ucControl/productcatedetail.php',
		type: 'POST',
		data: {id: data_id},
		success: function(data){
			//$('#formlist').load(document.URL +  '#formlist');
			var obj = JSON.parse(data);
			$("#txtIDChild").val(obj.ID);
			$("#txtNameChild").val(obj.Name);
			$("#txtPositionChild").val(obj.Position);
			$("#txtDescriptionChild").val(obj.Description);
			$("#txtKeyDescriptionChild").val(obj.MetaDescription);
			$("#txtKeywordChild").val(obj.MetaKeyword);
			if(obj.Status == 1)
			{
				$("#checkboxChild1_8").attr("checked","false");
				$("#checkboxChild1_9").attr("checked","true");
			}
			else
			{
				$("#checkboxChild1_8").attr("checked","true");
				$("#checkboxChild1_9").attr("checked","false");
			}
			
			$("#Input_CreatedByChild").val(obj.CreatedBy);
			$("#Input_CreatedDateChild").val(obj.CreatedDate == null ? "" : obj.CreatedDate);
			$("#Input_UpdatedByChild").val(obj.UpdatedBy);
			$("#Input_UpdatedDateChild").val(obj.UpdatedDate == null ? "" : obj.UpdatedDate);
			},
			error: function(jqXHR, textStatus, errorThrown){
				toastr["error"]("cập nhập trạng thái không thành công");
				
			}
		});
	});
	
	$('.btndelete').on('click', function(){
		var result = confirm("Bạn có muốn xóa danh mục này không?");
		if (result) {
			var data_id = $(this).data('id');
			$.ajax({
				url: 'ucControl/productcatedelete.php',
				type: 'POST',
				data: {id: data_id},
				success: function(data){
					window.location.reload();
					
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert("Xóa không thành công");
					
				}
			});
		}
	});
	
	
	$('.btndeletechildren').on('click', function(){
		var result = confirm("Bạn có muốn xóa danh mục này không?");
		if (result) {
			var data_id = $(this).data('id');
			$.ajax({
				url: 'ucControl/productcatedeletechild.php',
				type: 'POST',
				data: {id: data_id},
				success: function(data){
					window.location.reload();
					
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert("Xóa không thành công");
					
				}
			});
		}
	});
	
	
	$('.boxclick').on('click', function(e){
		 e.preventDefault();
        $(this).addClass('active').siblings().removeClass('active');
		
	});
					
});

$(document).ready(function(){
	$('.clickable').on('click', function(){
		var data_id = $(this).data('id');
		$.ajax({
			url: 'ucControl/accountstatus.php',
			type: 'POST',
			data: {id: data_id},
			success: function(data){
				//$('#formlist').load(document.URL +  '#formlist');
				if(data == 1)
				{
					$(this).removeClass('btn-danger');
					$(this).addClass('btn-success');
					toastr["success"]("cập nhập trạng thái thành công");
				}
				else
				{
					$(this).addClass('btn-danger');
					$(this).removeClass('btn-success');
					toastr["success"]("cập nhập trạng thái thành công");
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				toastr["error"]("cập nhập trạng thái không thành công");
				
			}
		});
	});


	$('.buttondelete').on('click', function(){
		var result = confirm("Bạn có muốn xóa tài khoản này không?");
		if (result) {
			var data_id = $(this).data('id');
			$.ajax({
				url: 'ucControl/accountdelete.php',
				type: 'POST',
				data: {id: data_id},
				success: function(data){
					$('#formlist').load(document.URL +  '#formlist');
					alert("Xóa thành công");
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert("Xóa không thành công");
					
				}
			});
		}
		
	});
});
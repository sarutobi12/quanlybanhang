$(document).ready(function(){
    $('#selectparent').on('change', function(){
        var data_id = document.getElementById("selectparent").value;
        $.ajax({
            type : 'POST',
            url  : 'ucControl/articlecate.php',
            data : {id: data_id},
            success :  function(data)
            {
                $("#catechild").html(data);
                if(data.length == 0)
                {
                    $("#childnum").attr("value","0");
                }
                else
                {
                    $("#childnum").attr("value","1");
                }
            }
        });
    });
});
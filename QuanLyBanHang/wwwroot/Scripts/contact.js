$(document).ready(function(){
    $("#btnsubmit").click(function() {
        var text = $("#txtSearch").val();
        var url = location.pathname;
        var txtsearch = location.search;
        var urllink = url + "?key=" + text+"&page=1";

        location.replace(urllink);
    });
    $('.clickable').on('click', function(){
        var data_id = $(this).data('id');
        $.ajax({
            url: 'ucControl/contactstatus.php',
            type: 'POST',
            data: {id: data_id},
            success: function(data){
                //$('#formlist').load(document.URL +  '#formlist');
                if(data == 1)
                {
                    location.reload();
                }
                else
                {
                    location.reload();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                toastr["error"]("cập nhập trạng thái không thành công");

            }
        });
    });


    $('.buttondelete').on('click', function(){
        var result = confirm("Bạn có muốn xóa thư liên hệ này không?");
        if (result) {
            var data_id = $(this).data('id');
            $.ajax({
                url: 'ucControl/maildelete.php',
                type: 'POST',
                data: {id: data_id},
                success: function(data){
                    $('#formlist').load(document.URL +  '#formlist');
                    toastr["success"]("cập nhập trạng thái xóa thành công");
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    toastr["error"]("cập nhập trạng thái không thành công");

                }
            });
        }

    });
});
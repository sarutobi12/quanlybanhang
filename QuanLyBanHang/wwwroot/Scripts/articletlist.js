$(document).ready(function(){
    $("#selectcatechild").on('change', function(){
        var mid = document.getElementById("selectparent").value;
        var cid = document.getElementById("selectcatechild").value;
        var url = location.pathname;
        var urllink = url + "?mid=" + mid + "&cid=" + cid;
        location.replace(urllink);
    });
    $("#btnsubmit").click(function() {
        var text = $("#txtSearch").val();
        var mid = document.getElementById("selectparent").value;
        var cid = document.getElementById("selectcatechild") != null ? document.getElementById("selectcatechild").value : '';
        var url = location.pathname;
        var txtsearch = location.search;
        var urllink = url + "?mid=" + mid + "&cid=" + cid + "&key=" + text;

        location.replace(urllink);
    });

    $('.clickable').on('click', function(){
        var data_id = $(this).data('id');
        $.ajax({
            url: 'ucControl/articlestatus.php',
            type: 'POST',
            data: {id: data_id},
            success: function(data){
                //$('#formlist').load(document.URL +  '#formlist');
                if(data == 1)
                {
                    $(this).removeClass('btn-danger');
                    $(this).addClass('btn-success');
                    toastr["success"]("cập nhập trạng thái thành công");
                    location.reload();
                }
                else
                {
                    $(this).addClass('btn-danger');
                    $(this).removeClass('btn-success');
                    toastr["success"]("cập nhập trạng thái thành công");
                    location.reload();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                toastr["error"]("cập nhập trạng thái không thành công");

            }
        });
    });


    $('.buttondelete').on('click', function(){
        var result = confirm("Bạn có muốn xóa sản phẩm này không?");
        if (result) {
            var data_id = $(this).data('id');
            $.ajax({
                url: 'ucControl/articledelete.php',
                type: 'POST',
                data: {id: data_id},
                success: function(data){
                    $('#formlist').load(document.URL +  '#formlist');
                    toastr["success"]("cập nhập trạng thái xóa thành công");
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown){
                    toastr["error"]("cập nhập trạng thái không thành công");

                }
            });
        }

    });
});
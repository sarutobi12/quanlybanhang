USE [DATNQuanLyNhaHang]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 4/12/2019 9:18:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Username] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[FullName] [nvarchar](50) NULL,
	[BirthDay] [datetime] NULL,
	[Address] [nvarchar](255) NULL,
	[Avatar] [nvarchar](255) NULL,
	[Mobi] [nvarchar](20) NULL,
	[Description] [nvarchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[CreateBy] [nvarchar](255) NULL,
	[Position] [int] NULL,
	[Status] [bit] NULL,
	[QRCodeHass] [nvarchar](500) NULL,
	[AccountMainID] [int] NULL,
 CONSTRAINT [PK__Account__536C85E598AFF8C3] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AccountMain]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountMain](
	[AccountMainID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[Avatar] [nvarchar](500) NULL,
	[Position] [int] NULL,
	[Status] [bit] NULL,
	[Default_Url] [nvarchar](50) NULL,
 CONSTRAINT [PK__AccountM__3ACB361773852DD3] PRIMARY KEY CLUSTERED 
(
	[AccountMainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bill]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[BillID] [int] IDENTITY(1,1) NOT NULL,
	[Position] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[Status] [bit] NULL,
	[Total] [float] NULL,
	[TotalAmount] [float] NULL,
	[Bonus] [int] NULL,
	[CreateBy] [nvarchar](255) NULL,
	[TableFoodID] [int] NULL,
 CONSTRAINT [PK__Bill__11F2FC4A93616E01] PRIMARY KEY CLUSTERED 
(
	[BillID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BillDetail]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillDetail](
	[BillID] [int] NOT NULL,
	[FoodID] [int] NOT NULL,
	[Amount] [float] NULL,
	[Quantity] [int] NULL,
 CONSTRAINT [PK__BillDeta__11F2FC4A32A97B8C] PRIMARY KEY CLUSTERED 
(
	[BillID] ASC,
	[FoodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Food]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Food](
	[FoodID] [int] IDENTITY(1,1) NOT NULL,
	[FoodName] [nvarchar](255) NULL,
	[Avatar] [nvarchar](255) NULL,
	[Price_Import] [float] NULL,
	[Price_Output] [float] NULL,
	[Quantity] [int] NULL,
	[Position] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[Status] [bit] NULL,
	[CreateBy] [nvarchar](255) NULL,
	[FoodTypeID] [int] NULL,
 CONSTRAINT [PK__Food__856DB3CBDE5235A4] PRIMARY KEY CLUSTERED 
(
	[FoodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FoodImport]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FoodImport](
	[FoodImportID] [int] IDENTITY(1,1) NOT NULL,
	[Total] [float] NULL,
	[CreateTime] [datetime] NULL,
	[CreateBy] [nvarchar](255) NULL,
 CONSTRAINT [PK_FoodImport] PRIMARY KEY CLUSTERED 
(
	[FoodImportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FoodImportDetail]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FoodImportDetail](
	[FoodImportID] [int] NOT NULL,
	[FoodID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Amount] [float] NULL,
 CONSTRAINT [PK_FoodImportDetail] PRIMARY KEY CLUSTERED 
(
	[FoodImportID] ASC,
	[FoodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FoodType]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FoodType](
	[FoodTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Avatar] [nvarchar](255) NULL,
	[Position] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[Status] [bit] NULL,
	[CreateBy] [nvarchar](255) NULL,
 CONSTRAINT [PK__FoodType__D3D1546C299F496F] PRIMARY KEY CLUSTERED 
(
	[FoodTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Menu]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Path] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	[Icon] [nvarchar](50) NULL,
	[Role] [int] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MenuAccountMain]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuAccountMain](
	[MenuID] [int] NOT NULL,
	[AccountMainID] [int] NOT NULL,
	[UpdateBy] [nvarchar](255) NULL,
	[UpdateTime] [datetime] NULL,
 CONSTRAINT [PK_RoleUser] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC,
	[AccountMainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderHistory]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FoodID] [int] NULL,
	[OldQuantity] [int] NULL,
	[TableID] [int] NULL,
	[NewQuantity] [int] NULL,
	[AccountOrder] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[CreateTime] [datetime] NULL,
	[Status] [bit] NULL,
	[NewTableID] [int] NULL,
 CONSTRAINT [PK_OrderHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
	[CreateBy] [nvarchar](255) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_Role1] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableFood]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableFood](
	[TableID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](255) NULL,
	[Avatar] [nvarchar](255) NULL,
	[Position] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[CreateTime] [datetime] NULL,
	[Status] [bit] NULL,
	[Total] [float] NULL,
 CONSTRAINT [PK__TableFoo__7D5F018E0812C4BA] PRIMARY KEY CLUSTERED 
(
	[TableID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableFoodDetail]    Script Date: 4/12/2019 9:18:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableFoodDetail](
	[TableFoodID] [int] NOT NULL,
	[FoodID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Amount] [float] NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_TableFoodDetail] PRIMARY KEY CLUSTERED 
(
	[TableFoodID] ASC,
	[FoodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'admin', N'7523C62ABDB7628C5A9DAD8F97D8D8C5C040EDE36535E531A8A3748B6CAE7E00', N'admin@gmail.com', N'Administrator', NULL, N'Thuận An - Bình Dương', NULL, N'0972332803', NULL, CAST(N'2019-03-29 13:37:35.510' AS DateTime), NULL, 0, 1, N'38BFC899A9408F995FFF503D4CDD11BA76EE1DEC871F1AA8AC8D1E526E27CBFA', 6)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'dothanhphong', N'578DB74EF4CAC5C445AE94DE445E3FC81EE2B2B7BA9E56FA5D1C68D7A6ED3F3B', N'dothanhphong@gmail.com', N'Trần Đỗ Thanh Phong', NULL, N'Tân Ba - Bình Dương', NULL, N'098568967', NULL, CAST(N'2019-03-29 13:44:37.923' AS DateTime), NULL, 0, 1, N'FD035C0F11B30CFEA398346BA8DCAA42D111678AADB320DD5ECFBA171459FCCB', 10)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'hoangholan', N'578DB74EF4CAC5C445AE94DE445E3FC81EE2B2B7BA9E56FA5D1C68D7A6ED3F3B', N'hoangholan@gmail.com', N'Hồ Thị Kiều Lan', NULL, N'Lộc Ninh- Bình Phước', NULL, N'0124567535', NULL, CAST(N'2019-03-29 13:43:28.430' AS DateTime), NULL, 0, 1, N'8EE14FD494373CA004906DF83F0A2AFEE0E57ADF5958C144080955D4FD5A952B', 7)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'levannhi', N'7523C62ABDB7628C5A9DAD8F97D8D8C5C040EDE36535E531A8A3748B6CAE7E00', N'vannhi@gmail.com', N'Lê Văn Nhị', NULL, N'Vô Danh  - Ẩn Tích', NULL, N'0168456975', NULL, CAST(N'2019-03-29 13:48:28.563' AS DateTime), N'vanquyenktktbd', 0, 1, N'0899E87F1ED3EAD8F3F59E634D4BCFC0C9339F47BCC22A3A080B301830BE471E', 8)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'ngonguyenkyphuong', N'4FBA7B05F1CA5CA810D7757DDA4929D981974E8BFB3D0F432C064AEEF14459D0', N'ngonguyenkyphuong@gmail.com', N'Ngô Nguyễn Kỳ Phương', NULL, N'Bến Cát - Bình Dương', NULL, N'0168715684', NULL, CAST(N'2019-03-29 13:45:36.640' AS DateTime), NULL, 0, 1, N'E309896C51D9ED77CE7C16102BF929895BB175D5D06519C45A928E2AA55D692C', 9)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'nguyenhoangnam', N'578DB74EF4CAC5C445AE94DE445E3FC81EE2B2B7BA9E56FA5D1C68D7A6ED3F3B', N'nguyenhoangnam@gmail.com', N'Nguyễn Hoàng Nam', NULL, N'Bến Cát - Bình Dương', NULL, N'0163254698', NULL, CAST(N'2019-03-29 13:50:57.230' AS DateTime), NULL, 0, 1, N'C53ECA12DC61B02A8FB2E6683325C6A1E20A44DB33D75FBCF0BC6305E8B5C2D8', 7)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'vanquyen', N'578DB74EF4CAC5C445AE94DE445E3FC81EE2B2B7BA9E56FA5D1C68D7A6ED3F3B', N'abc@gmail.com', N'Quyền 2', NULL, N'Bình Phước', NULL, N'0373569752', NULL, CAST(N'2019-04-02 11:25:53.817' AS DateTime), NULL, 0, 1, N'48285A58F414E0F1C8CBF3698235D825C9DBE362AA7115FC06C4335941F8C427', 8)
INSERT [dbo].[Account] ([Username], [Password], [Email], [FullName], [BirthDay], [Address], [Avatar], [Mobi], [Description], [CreateTime], [CreateBy], [Position], [Status], [QRCodeHass], [AccountMainID]) VALUES (N'vanquyenktktbd', N'578DB74EF4CAC5C445AE94DE445E3FC81EE2B2B7BA9E56FA5D1C68D7A6ED3F3B', N'vanquyenktktbd@gmail.com', N'Nguyễn Văn Quyền', NULL, N'Thuận An - Bình Dương', NULL, N'0972332803', NULL, CAST(N'2019-03-29 13:37:35.510' AS DateTime), N'admin', 0, 1, N'4B4B9DE1BCFA7560C610026ABC6552430B51013897C241713F50E3EA2E537800', 6)
SET IDENTITY_INSERT [dbo].[AccountMain] ON 

INSERT [dbo].[AccountMain] ([AccountMainID], [Title], [Description], [CreateTime], [Avatar], [Position], [Status], [Default_Url]) VALUES (6, N'Quản trị viên cấp cao', N'Quản trị viên cấp cao', CAST(N'2019-03-12 00:00:00.000' AS DateTime), N'1', 0, 1, N'/')
INSERT [dbo].[AccountMain] ([AccountMainID], [Title], [Description], [CreateTime], [Avatar], [Position], [Status], [Default_Url]) VALUES (7, N'Nhân viên quản lý', NULL, NULL, NULL, 0, 1, N'/')
INSERT [dbo].[AccountMain] ([AccountMainID], [Title], [Description], [CreateTime], [Avatar], [Position], [Status], [Default_Url]) VALUES (8, N'Nhân viên order', NULL, NULL, NULL, 4, 1, N'/#/table-list')
INSERT [dbo].[AccountMain] ([AccountMainID], [Title], [Description], [CreateTime], [Avatar], [Position], [Status], [Default_Url]) VALUES (9, N'Nhân viên pha chế', NULL, CAST(N'2019-03-29 09:29:30.967' AS DateTime), NULL, 5, 1, N'/')
INSERT [dbo].[AccountMain] ([AccountMainID], [Title], [Description], [CreateTime], [Avatar], [Position], [Status], [Default_Url]) VALUES (10, N'Nhân viên kho', NULL, CAST(N'2019-04-01 23:27:50.133' AS DateTime), NULL, 3, 1, N'/')
SET IDENTITY_INSERT [dbo].[AccountMain] OFF
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (92, NULL, NULL, CAST(N'2019-04-03 12:38:01.813' AS DateTime), 1, 95000, 95000, 0, N'admin', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (93, NULL, NULL, CAST(N'2019-04-03 12:38:33.757' AS DateTime), 1, 55000, 49500, 10, N'admin', 7)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (94, NULL, NULL, CAST(N'2019-04-03 12:38:44.667' AS DateTime), 1, 15000, 15000, 0, N'admin', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (95, NULL, NULL, CAST(N'2019-04-03 12:39:05.657' AS DateTime), 1, 560000, 560000, 0, N'admin', 7)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (96, NULL, NULL, CAST(N'2019-04-02 12:39:17.113' AS DateTime), 1, 25000, 0, 100, N'admin', 5)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (97, NULL, NULL, CAST(N'2019-04-02 12:39:30.120' AS DateTime), 1, 60000, 0, 100, N'admin', 8)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (98, NULL, NULL, CAST(N'2019-04-03 12:42:05.607' AS DateTime), 1, 20000, 20000, 0, N'admin', 5)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (99, NULL, NULL, CAST(N'2019-04-01 12:42:22.890' AS DateTime), 1, 110000, 110000, 0, N'admin', 7)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (100, NULL, NULL, CAST(N'2019-04-03 14:01:58.430' AS DateTime), 1, 55000, 55000, 0, N'hoangholan', 3)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (101, NULL, NULL, CAST(N'2019-04-03 14:02:11.750' AS DateTime), 1, 90000, 81000, 10, N'hoangholan', 5)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (102, NULL, NULL, CAST(N'2019-04-02 14:21:57.017' AS DateTime), 1, 70000, 70000, 0, N'vanquyenktktbd', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (103, NULL, NULL, CAST(N'2019-04-07 17:17:15.040' AS DateTime), 1, 750000, 750000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (104, NULL, NULL, CAST(N'2019-04-07 17:18:01.157' AS DateTime), 1, 150000, 150000, 0, N'admin', 7)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (105, NULL, NULL, CAST(N'2019-04-01 17:18:39.187' AS DateTime), 1, 1500000, 1500000, 0, N'admin', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (106, NULL, NULL, CAST(N'2019-04-07 17:20:52.010' AS DateTime), 1, 600000, 600000, 0, N'admin', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (107, NULL, NULL, CAST(N'2019-04-07 17:37:48.170' AS DateTime), 1, 30000, 30000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (108, NULL, NULL, CAST(N'2019-04-07 21:16:57.163' AS DateTime), 1, 195000, 195000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (109, NULL, NULL, CAST(N'2019-04-07 22:19:20.297' AS DateTime), 1, 60000, 60000, 0, N'admin', 3)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (110, NULL, NULL, CAST(N'2019-04-08 19:53:06.687' AS DateTime), 1, 30000, 30000, 0, N'hoangholan', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (111, NULL, NULL, CAST(N'2019-04-08 19:53:31.363' AS DateTime), 1, 10000, 10000, 0, N'hoangholan', 4)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (112, NULL, NULL, CAST(N'2019-04-08 21:58:36.353' AS DateTime), 1, 60000, 60000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (113, NULL, NULL, CAST(N'2019-04-08 21:58:44.403' AS DateTime), 1, 30000, 30000, 0, N'admin', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (114, NULL, NULL, CAST(N'2019-04-09 20:17:55.977' AS DateTime), 1, 30000, 30000, 0, N'admin', 3)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (115, NULL, NULL, CAST(N'2019-04-09 20:18:00.543' AS DateTime), 1, 140000, 140000, 0, N'admin', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (116, NULL, NULL, CAST(N'2019-04-09 20:18:06.223' AS DateTime), 1, 105000, 105000, 0, N'admin', 8)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (117, NULL, NULL, CAST(N'2019-04-09 21:31:17.900' AS DateTime), 1, 30000, 30000, 0, N'admin', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (118, NULL, NULL, CAST(N'2019-04-09 21:31:22.807' AS DateTime), 1, 15000, 15000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (119, NULL, NULL, CAST(N'2019-04-09 21:31:28.217' AS DateTime), 1, 85000, 85000, 0, N'admin', 3)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (120, NULL, NULL, CAST(N'2019-04-09 21:31:33.977' AS DateTime), 1, 60000, 60000, 0, N'admin', 9)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (121, NULL, NULL, CAST(N'2019-04-09 21:31:41.870' AS DateTime), 1, 60000, 60000, 0, N'admin', 12)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (122, NULL, NULL, CAST(N'2019-04-09 21:35:26.533' AS DateTime), 1, 45000, 45000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (123, NULL, NULL, CAST(N'2019-04-09 21:37:15.000' AS DateTime), 1, 15000, 15000, 0, N'admin', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (124, NULL, NULL, CAST(N'2019-04-09 22:36:03.187' AS DateTime), 1, 25000, 25000, 0, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (125, NULL, NULL, CAST(N'2019-04-09 22:40:14.103' AS DateTime), 1, 55000, 55000, 0, N'hoangholan', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (126, NULL, NULL, CAST(N'2019-04-09 22:40:21.360' AS DateTime), 1, 30000, 30000, 0, N'hoangholan', 7)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (127, NULL, NULL, CAST(N'2019-04-09 22:57:23.517' AS DateTime), 1, 60000, 60000, 0, N'levannhi', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (128, NULL, NULL, CAST(N'2019-04-09 22:57:32.643' AS DateTime), 1, 105000, 105000, 0, N'levannhi', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (129, NULL, NULL, CAST(N'2019-04-10 19:13:46.947' AS DateTime), 1, 30000, 30000, 0, N'admin', 1)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (130, NULL, NULL, CAST(N'2019-04-10 19:15:48.813' AS DateTime), 1, 30000, 27000, 10, N'admin', 2)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (131, NULL, NULL, CAST(N'2019-04-10 20:22:58.273' AS DateTime), 1, 105000, 105000, 0, N'admin', 4)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (132, NULL, NULL, CAST(N'2019-04-10 20:30:57.743' AS DateTime), 1, 60000, 60000, 0, N'hoangholan', 6)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (133, NULL, NULL, CAST(N'2019-04-10 20:31:27.577' AS DateTime), 1, 45000, 45000, 0, N'hoangholan', 5)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (134, NULL, NULL, CAST(N'2019-04-10 20:33:14.023' AS DateTime), 1, 15000, 15000, 0, N'hoangholan', 8)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (135, NULL, NULL, CAST(N'2019-04-10 20:33:23.057' AS DateTime), 1, 15000, 15000, 0, N'hoangholan', 9)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (136, NULL, NULL, CAST(N'2019-04-10 20:34:58.587' AS DateTime), 1, 100000, 100000, 0, N'hoangholan', 12)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (137, NULL, NULL, CAST(N'2019-04-10 20:35:05.847' AS DateTime), 1, 30000, 30000, 0, N'hoangholan', 13)
INSERT [dbo].[Bill] ([BillID], [Position], [Description], [CreateTime], [Status], [Total], [TotalAmount], [Bonus], [CreateBy], [TableFoodID]) VALUES (138, NULL, NULL, CAST(N'2019-04-10 22:31:46.533' AS DateTime), 1, 25000, 25000, 0, N'admin', 2)
SET IDENTITY_INSERT [dbo].[Bill] OFF
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (92, 33, 50000, 5)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (92, 35, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (92, 37, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (93, 32, 30000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (93, 33, 10000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (93, 35, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (94, 35, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (95, 3, 500000, 5)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (95, 32, 60000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (96, 23, 25000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (97, 32, 60000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (98, 2, 20000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (99, 23, 50000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (99, 32, 60000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (100, 32, 30000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (100, 33, 10000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (100, 35, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (101, 22, 90000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (102, 23, 25000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (102, 32, 30000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (102, 35, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (103, 38, 750000, 50)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (104, 38, 150000, 10)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (105, 32, 1500000, 50)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (106, 32, 600000, 20)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (107, 2, 30000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (108, 35, 45000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (108, 37, 75000, 5)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (108, 38, 75000, 5)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (109, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (109, 38, 45000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (110, 38, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (111, 33, 10000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (112, 38, 60000, 4)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (113, 37, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (114, 38, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (115, 33, 20000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (115, 37, 45000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (115, 38, 75000, 5)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (116, 35, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (116, 37, 75000, 5)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (117, 38, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (118, 38, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (119, 23, 25000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (119, 37, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (119, 38, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (120, 38, 60000, 4)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (121, 38, 60000, 4)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (122, 38, 45000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (123, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (124, 33, 10000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (124, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (125, 4, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (125, 23, 25000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (126, 32, 30000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (127, 32, 30000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (127, 37, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (128, 32, 60000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (128, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (128, 38, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (129, 32, 30000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (130, 37, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (131, 35, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (131, 37, 30000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (131, 38, 60000, 4)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (132, 37, 60000, 4)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (133, 38, 45000, 3)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (134, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (135, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (136, 22, 60000, 2)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (136, 33, 40000, 4)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (137, 37, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (137, 38, 15000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (138, 33, 10000, 1)
INSERT [dbo].[BillDetail] ([BillID], [FoodID], [Amount], [Quantity]) VALUES (138, 37, 15000, 1)
SET IDENTITY_INSERT [dbo].[Food] ON 

INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (2, N'Cà phê đen nóng', NULL, 8000, 10000, 50, 2, N'Ly', NULL, 1, N'admin', 7)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (3, N'Sting', NULL, 7000, 100000, 22, 3, N'Chai', NULL, 1, N'admin', 3)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (4, N'Bò cụng', NULL, 10000, 15000, 4, 4, N'Lon', NULL, 1, N'admin', 3)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (22, N'Trà sữa thái', NULL, 10000, 30000, 40, NULL, N'Ly', CAST(N'2019-03-25 13:57:46.753' AS DateTime), 1, N'admin', 5)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (23, N'Trà sữa thập cẩm ', NULL, 15000, 25000, 89, NULL, N'Ly', CAST(N'2019-03-25 13:58:08.460' AS DateTime), 1, N'admin', 5)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (32, N'Gà rán', NULL, 15000, 30000, 50, NULL, N'Cái', CAST(N'2019-03-26 13:03:18.887' AS DateTime), 1, N'admin', 4)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (33, N'C2', NULL, 7000, 10000, 33, NULL, N'Chai', CAST(N'2019-03-26 13:10:04.923' AS DateTime), 1, N'admin', 3)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (35, N'Trâu cụng', NULL, 12000, 15000, 0, NULL, N'Lon', CAST(N'2019-03-26 13:40:57.380' AS DateTime), 1, N'admin', 3)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (37, N'Sinh tố bơ', NULL, 10000, 15000, 2, NULL, N'ly', CAST(N'2019-04-01 19:14:11.130' AS DateTime), 1, N'admin', 6)
INSERT [dbo].[Food] ([FoodID], [FoodName], [Avatar], [Price_Import], [Price_Output], [Quantity], [Position], [Description], [CreateTime], [Status], [CreateBy], [FoodTypeID]) VALUES (38, N'Ca cao đá nóng', NULL, 10000, 15000, 37, NULL, N'Ly', CAST(N'2019-04-07 17:16:33.930' AS DateTime), 1, N'admin', 10)
SET IDENTITY_INSERT [dbo].[Food] OFF
SET IDENTITY_INSERT [dbo].[FoodImport] ON 

INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (1, 110000, CAST(N'2019-03-27 19:32:31.857' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (2, 2421000, CAST(N'2019-03-27 19:36:21.523' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (3, 525000, CAST(N'2019-03-27 20:13:55.450' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (4, 98000, CAST(N'2019-03-25 20:43:37.400' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (5, 3000000, CAST(N'2019-03-27 21:56:34.913' AS DateTime), N'dothanhphong')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (6, 45000, CAST(N'2019-03-27 22:43:24.780' AS DateTime), N'dothanhphong')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (7, 1363000, CAST(N'2019-03-29 20:43:01.150' AS DateTime), N'hoangholan')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (8, 3000000, CAST(N'2019-03-31 09:56:48.407' AS DateTime), N'dothanhphong')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (9, 600000, CAST(N'2019-03-31 09:57:14.377' AS DateTime), N'hoangholan')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (10, 7000, CAST(N'2019-03-31 11:17:39.363' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (11, 3560000, CAST(N'2019-03-31 13:56:40.307' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (12, 1650000, CAST(N'2019-03-31 16:12:31.053' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (13, 710000, CAST(N'2019-04-01 19:15:23.227' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (14, 10000, CAST(N'2019-04-01 19:16:55.603' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (15, 7000, CAST(N'2019-04-02 10:18:26.780' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (16, 12000, CAST(N'2019-04-02 11:42:47.637' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (17, 10000, CAST(N'2019-04-02 15:08:03.830' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (18, 15000, CAST(N'2019-04-02 15:12:03.883' AS DateTime), N'vanquyenktktbd')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (19, 15000, CAST(N'2019-04-03 14:03:50.100' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (20, 500000, CAST(N'2019-04-07 17:16:52.687' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (21, 500000, CAST(N'2019-04-07 17:17:47.257' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (22, 12000, CAST(N'2019-04-09 20:09:15.393' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (23, 500000, CAST(N'2019-04-09 22:24:49.257' AS DateTime), N'admin')
INSERT [dbo].[FoodImport] ([FoodImportID], [Total], [CreateTime], [CreateBy]) VALUES (24, 400000, CAST(N'2019-04-09 22:25:32.523' AS DateTime), N'admin')
SET IDENTITY_INSERT [dbo].[FoodImport] OFF
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (1, 3, 5, 35000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (1, 32, 5, 75000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (2, 2, 12, 96000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (2, 23, 35, 525000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (2, 33, 30, 1800000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (3, 22, 5, 0)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (3, 23, 5, 75000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (3, 32, 10, 150000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (3, 33, 5, 300000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (4, 2, 1, 8000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (4, 23, 5, 75000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (4, 32, 1, 15000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (5, 33, 50, 3000000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (6, 22, 1, 0)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (6, 23, 2, 30000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (6, 32, 1, 15000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (7, 3, 19, 133000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (7, 4, 123, 1230000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (8, 33, 50, 3000000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (9, 35, 50, 600000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (10, 3, 1, 7000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (11, 4, 50, 500000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (11, 33, 51, 3060000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (12, 23, 1, 15000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (12, 32, 101, 1515000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (12, 33, 2, 120000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (13, 3, 30, 210000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (13, 37, 50, 500000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (14, 4, 1, 10000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (15, 3, 1, 7000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (16, 35, 1, 12000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (17, 37, 1, 10000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (18, 32, 1, 15000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (19, 23, 1, 15000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (20, 38, 50, 500000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (21, 38, 50, 500000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (22, 35, 1, 12000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (23, 38, 50, 500000)
INSERT [dbo].[FoodImportDetail] ([FoodImportID], [FoodID], [Quantity], [Amount]) VALUES (24, 2, 50, 400000)
SET IDENTITY_INSERT [dbo].[FoodType] ON 

INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (3, N'Nước giải khát', NULL, 2, NULL, NULL, 1, N'admin')
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (4, N'Thức ăn nhanh', NULL, 6, NULL, CAST(N'2019-03-29 20:52:17.310' AS DateTime), 1, N'admin')
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (5, N'Trà sữa', NULL, 2, NULL, CAST(N'2019-03-29 20:52:31.760' AS DateTime), 1, NULL)
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (6, N'Sinh tố', NULL, 2, NULL, CAST(N'2019-03-23 07:40:03.067' AS DateTime), 1, NULL)
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (7, N'Cà phê', NULL, 3, NULL, CAST(N'2019-04-02 14:44:59.500' AS DateTime), 1, NULL)
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (8, N'Chè', NULL, 6, NULL, CAST(N'2019-04-02 14:42:40.667' AS DateTime), 1, NULL)
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (9, N'Nước ép', NULL, 7, NULL, CAST(N'2019-04-02 14:44:19.797' AS DateTime), 1, NULL)
INSERT [dbo].[FoodType] ([FoodTypeID], [Name], [Avatar], [Position], [Description], [CreateTime], [Status], [CreateBy]) VALUES (10, N'Ca cao', NULL, 8, NULL, CAST(N'2019-04-02 14:44:52.290' AS DateTime), 1, NULL)
SET IDENTITY_INSERT [dbo].[FoodType] OFF
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([ID], [Title], [Name], [Path], [Status], [Icon], [Role]) VALUES (1, N'Bàn làm việc', N'Home', N'/', 1, N'fa fa-dashboard', 7)
INSERT [dbo].[Menu] ([ID], [Title], [Name], [Path], [Status], [Icon], [Role]) VALUES (2, N'Quản lý hàng hóa', N'product', N'/product', 1, N'fa fa-th', 1)
INSERT [dbo].[Menu] ([ID], [Title], [Name], [Path], [Status], [Icon], [Role]) VALUES (3, N'Quản lý bàn', N'table-manager', N'/table-manager', 1, N'fa fa-table', 2)
INSERT [dbo].[Menu] ([ID], [Title], [Name], [Path], [Status], [Icon], [Role]) VALUES (4, N'Quản lý nhân sự', N'account', N'/account', 1, N'fa fa-user', 3)
INSERT [dbo].[Menu] ([ID], [Title], [Name], [Path], [Status], [Icon], [Role]) VALUES (5, N'Nhập hàng', N'import', N'/import', 1, N'fa fa-edit', 4)
INSERT [dbo].[Menu] ([ID], [Title], [Name], [Path], [Status], [Icon], [Role]) VALUES (6, N'Thống kê', N'report', NULL, 1, N'fa fa-pie-chart', 5)
SET IDENTITY_INSERT [dbo].[Menu] OFF
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (1, 6, N'admin', CAST(N'2019-04-02 09:06:26.547' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (1, 7, N'admin', CAST(N'2019-04-02 09:07:08.143' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (2, 6, N'admin', CAST(N'2019-04-02 09:06:26.737' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (2, 7, N'admin', CAST(N'2019-04-02 09:07:08.143' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (3, 6, N'admin', CAST(N'2019-04-02 09:06:26.740' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (3, 7, N'admin', CAST(N'2019-04-02 09:07:08.143' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (4, 6, N'admin', CAST(N'2019-04-02 09:06:26.740' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (5, 6, N'admin', CAST(N'2019-04-02 09:06:26.743' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (5, 10, N'admin', CAST(N'2019-04-02 11:23:01.367' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (6, 6, N'admin', CAST(N'2019-04-02 09:06:26.743' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (6, 7, N'admin', CAST(N'2019-04-02 09:07:08.143' AS DateTime))
INSERT [dbo].[MenuAccountMain] ([MenuID], [AccountMainID], [UpdateBy], [UpdateTime]) VALUES (6, 10, N'admin', CAST(N'2019-04-02 11:23:01.370' AS DateTime))
SET IDENTITY_INSERT [dbo].[OrderHistory] ON 

INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (3, 38, 0, 3, 1, N'admin', N'add', CAST(N'2019-04-09 21:35:38.970' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (4, 38, 1, 3, 3, N'admin', N'edit', CAST(N'2019-04-09 21:35:59.280' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (5, 37, 0, 3, 1, N'admin', N'add', CAST(N'2019-04-09 21:36:23.003' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (6, 35, 0, 3, 1, N'admin', N'add', CAST(N'2019-04-09 21:36:46.877' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (7, 37, 0, 1, 1, N'admin', N'add', CAST(N'2019-04-09 21:36:59.220' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (8, 37, 0, 1, 1, N'admin', N'add', CAST(N'2019-04-09 21:37:23.900' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (9, 33, 0, 2, 1, N'admin', N'add', CAST(N'2019-04-09 21:55:46.273' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (10, 37, 0, 2, 1, N'admin', N'add', CAST(N'2019-04-09 21:56:19.727' AS DateTime), 1, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (11, 32, 0, 7, 1, N'admin', N'add', CAST(N'2019-04-09 22:15:50.490' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (12, 23, 0, 6, 1, N'hoangholan', N'add', CAST(N'2019-04-09 22:16:20.967' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (13, 4, 0, 6, 2, N'hoangholan', N'add', CAST(N'2019-04-09 22:16:28.237' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (14, 37, 0, 8, 1, N'hoangholan', N'add', CAST(N'2019-04-09 22:18:52.710' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (15, 37, 1, 3, 2, N'dothanhphong', N'edit', CAST(N'2019-04-09 22:20:36.167' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (16, 37, 0, 2, 1, N'admin', N'add', CAST(N'2019-04-09 22:36:41.637' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (17, 38, 0, 5, 1, N'admin', N'add', CAST(N'2019-04-09 22:37:33.017' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (18, 38, 0, 4, 1, N'hoangholan', N'add', CAST(N'2019-04-09 22:39:21.460' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (19, 37, 1, 2, 2, N'hoangholan', N'edit', CAST(N'2019-04-09 22:39:33.823' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (20, 38, 1, 5, 2, N'dothanhphong', N'edit', CAST(N'2019-04-09 22:41:42.163' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (21, 38, 2, 5, 3, N'dothanhphong', N'edit', CAST(N'2019-04-09 22:41:57.660' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (22, 37, 0, 6, 1, N'hoangholan', N'add', CAST(N'2019-04-09 22:50:13.953' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (23, 37, 1, 6, 2, N'hoangholan', N'edit', CAST(N'2019-04-09 22:51:51.267' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (24, 32, 0, 6, 1, N'hoangholan', N'add', CAST(N'2019-04-09 22:51:56.383' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (25, 22, 0, 11, 1, N'hoangholan', N'add', CAST(N'2019-04-09 22:52:12.687' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (26, 22, 1, 11, 2, N'hoangholan', N'edit', CAST(N'2019-04-09 22:52:24.393' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (27, 37, 0, 9, 1, N'levannhi', N'add', CAST(N'2019-04-09 22:55:50.927' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (28, 32, 0, 1, 1, N'levannhi', N'add', CAST(N'2019-04-09 22:56:45.337' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (29, 38, 0, 1, 1, N'levannhi', N'add', CAST(N'2019-04-09 22:56:50.070' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (30, 38, 1, 1, 2, N'levannhi', N'edit', CAST(N'2019-04-09 22:56:54.493' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (31, 32, 1, 1, 2, N'levannhi', N'edit', CAST(N'2019-04-09 22:56:56.670' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (32, 32, 0, 1, 1, N'levannhi', N'add', CAST(N'2019-04-09 22:57:49.317' AS DateTime), 1, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (33, 38, 0, 1, 1, N'admin', N'add', CAST(N'2019-04-10 19:46:38.203' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (34, 37, 0, 1, 1, N'admin', N'add', CAST(N'2019-04-10 19:46:41.953' AS DateTime), 1, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (35, 37, 0, 6, 1, N'hoangholan', N'add', CAST(N'2019-04-10 19:47:11.857' AS DateTime), 1, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (36, 37, 1, 6, 4, N'hoangholan', N'edit', CAST(N'2019-04-10 19:47:16.823' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (37, 33, 0, 12, 4, N'hoangholan', N'add', CAST(N'2019-04-10 19:47:22.620' AS DateTime), 1, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (38, NULL, NULL, 1, NULL, N'admin', N'change', CAST(N'2019-04-10 20:22:05.237' AS DateTime), 0, 13)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (39, NULL, NULL, 3, NULL, N'admin', N'general', CAST(N'2019-04-10 20:22:44.330' AS DateTime), 0, 4)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (40, NULL, NULL, 4, NULL, N'admin', N'payment', CAST(N'2019-04-10 20:22:58.307' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (41, NULL, NULL, 6, NULL, N'hoangholan', N'payment', CAST(N'2019-04-10 20:30:58.100' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (42, NULL, NULL, 5, NULL, N'hoangholan', N'payment', CAST(N'2019-04-10 20:31:27.587' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (43, NULL, NULL, 8, NULL, N'hoangholan', N'payment', CAST(N'2019-04-10 20:33:14.037' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (44, NULL, NULL, 9, NULL, N'hoangholan', N'payment', CAST(N'2019-04-10 20:33:23.073' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (45, NULL, NULL, 11, NULL, N'hoangholan', N'change', CAST(N'2019-04-10 20:33:44.790' AS DateTime), 0, 1)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (46, NULL, NULL, 1, NULL, N'hoangholan', N'general', CAST(N'2019-04-10 20:33:56.277' AS DateTime), 0, 12)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (47, NULL, NULL, 12, NULL, N'hoangholan', N'payment', CAST(N'2019-04-10 20:34:58.600' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (48, NULL, NULL, 13, NULL, N'hoangholan', N'payment', CAST(N'2019-04-10 20:35:05.867' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (49, 37, 0, 2, 1, N'admin', N'add', CAST(N'2019-04-10 21:47:54.430' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (50, 33, 0, 2, 1, N'admin', N'add', CAST(N'2019-04-10 21:47:59.137' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (51, NULL, NULL, 2, NULL, N'admin', N'payment', CAST(N'2019-04-10 22:31:46.567' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (52, 38, 0, 2, 1, N'admin', N'add', CAST(N'2019-04-12 20:09:04.167' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (53, 37, 0, 6, 1, N'admin', N'add', CAST(N'2019-04-12 20:09:27.010' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (54, 37, 1, 6, 3, N'admin', N'edit', CAST(N'2019-04-12 20:09:35.037' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (55, 38, 1, 2, 6, N'admin', N'edit', CAST(N'2019-04-12 20:14:20.017' AS DateTime), 0, NULL)
INSERT [dbo].[OrderHistory] ([ID], [FoodID], [OldQuantity], [TableID], [NewQuantity], [AccountOrder], [Description], [CreateTime], [Status], [NewTableID]) VALUES (56, 33, 0, 4, 4, N'admin', N'add', CAST(N'2019-04-12 20:14:58.237' AS DateTime), 0, NULL)
SET IDENTITY_INSERT [dbo].[OrderHistory] OFF
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (1, N'Quản lý sản phẩm', N'admin', NULL)
INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (2, N'Quản lý bàn', N'admin', NULL)
INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (3, N'Quản lý tài khoản', N'admin', NULL)
INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (4, N'Nhập hàng', N'admin', NULL)
INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (5, N'Xem thống kê', N'admin', NULL)
INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (6, N'Bán hàng', N'admin', NULL)
INSERT [dbo].[Role] ([RoleID], [RoleName], [CreateBy], [CreateTime]) VALUES (7, N'Trang Quản lý', N'admin', NULL)
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[TableFood] ON 

INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (1, N'Bàn 1', NULL, 1, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (2, N'Bàn 2 ', NULL, 2, NULL, NULL, 1, 90000)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (3, N'Bàn 3', NULL, 3, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (4, N'Bàn 4', NULL, 4, NULL, NULL, 1, 40000)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (5, N'Bàn 5', NULL, 5, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (6, N'Bàn 6', NULL, 6, NULL, NULL, 1, 45000)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (7, N'Bàn 7', NULL, 7, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (8, N'Bàn 8', NULL, 8, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (9, N'Bàn 9', NULL, 9, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (10, N'Bàn 10', NULL, 10, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (11, N'Bàn 11', NULL, 11, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (12, N'Bàn 12', NULL, 12, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (13, N'Bàn 13', NULL, 13, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (14, N'Bàn 14', NULL, 14, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (15, N'Bàn 15', NULL, 15, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (24, N'Bàn 16', NULL, 16, NULL, CAST(N'2019-03-18 22:02:27.220' AS DateTime), 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (27, N'Bàn 17', NULL, 17, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (28, N'Bàn 18', NULL, 18, NULL, NULL, 0, 0)
INSERT [dbo].[TableFood] ([TableID], [TableName], [Avatar], [Position], [Description], [CreateTime], [Status], [Total]) VALUES (29, N'Bàn 19', NULL, 19, NULL, NULL, 0, 0)
SET IDENTITY_INSERT [dbo].[TableFood] OFF
INSERT [dbo].[TableFoodDetail] ([TableFoodID], [FoodID], [Quantity], [Amount], [Description]) VALUES (2, 38, 6, 90000, N'')
INSERT [dbo].[TableFoodDetail] ([TableFoodID], [FoodID], [Quantity], [Amount], [Description]) VALUES (4, 33, 4, 40000, N'')
INSERT [dbo].[TableFoodDetail] ([TableFoodID], [FoodID], [Quantity], [Amount], [Description]) VALUES (6, 37, 3, 45000, N'')
